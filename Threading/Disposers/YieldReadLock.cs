﻿using System;

namespace Pfz.Threading.Disposers
{
	/// <summary>
	/// Class returned by ReadLock method.
	/// </summary>
	public struct YieldReadLock:
		IDisposable
	{
		private YieldReaderWriterLock _lock;
		internal YieldReadLock(YieldReaderWriterLock yieldLock)
		{
			_lock = yieldLock;
		}

		/// <summary>
		/// Releases the lock.
		/// </summary>
		public void Dispose()
		{
			var yieldLock = _lock;

			if (yieldLock != null)
			{
				_lock = null;
				yieldLock._lock.ExitReadLock();
			}
		}
	}
}
