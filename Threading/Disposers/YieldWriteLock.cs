﻿using System;

namespace Pfz.Threading.Disposers
{
	/// <summary>
	/// Class returned by WriteLock method.
	/// </summary>
	public struct YieldWriteLock:
		IDisposable
	{
		private YieldReaderWriterLock _lock;
		internal YieldWriteLock(YieldReaderWriterLock yieldLock)
		{
			_lock = yieldLock;
		}

		/// <summary>
		/// Releases the lock.
		/// </summary>
		public void Dispose()
		{
			var yieldLock = _lock;

			if (yieldLock != null)
			{
				_lock = null;
				yieldLock._lock.ExitWriteLock();
			}
		}
	}
}
