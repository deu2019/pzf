﻿using System.Collections;

namespace Pfz.Threading.Cooperative
{
	/// <summary>
	/// Interface implemented by the generic version of the StackSaver, so it can be seen
	/// "untyped" and used by methods that may generate the right values without knowing exactly what
	/// type is expected.
	/// </summary>
	public interface IValueProducerStackSaver:
		IEnumerable,
		IEnumerator
	{
		/// <summary>
		/// Returns a value to the caller, but allows this method to continue later.
		/// </summary>
		void YieldReturn(object value);
	}
}
