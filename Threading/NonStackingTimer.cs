﻿using System;
using Pfz.Threading.Unsafe;

namespace Pfz.Threading
{
	/// <summary>
	/// Class that creates a Timer that runs an action at given intervals.
	/// The intervals are always counted after the end of the last action, so it does not stack or tries to
	/// compensate delays.
	/// </summary>
	public sealed class NonStackingTimer:
		DisposeAssurerBase<UnsafeNonStackingTimer>
	{
		/// <summary>
		/// Creates a new timer that will run the given action at the given interval milliseconds.
		/// If exclusiveThreadName is not null, then a new Thread will be created. Otherwise, a 
		/// UnlimitedThreadPool thread will be used.
		/// </summary>
		public NonStackingTimer(Action action, int interval, bool runImmediatelly=false, string exclusiveThreadName=null):
			base(new UnsafeNonStackingTimer(action, interval, runImmediatelly, exclusiveThreadName))
		{
		}

		/// <summary>
		/// Gets the action that's run by this timer.
		/// </summary>
		public Action Action
		{
			get
			{
				return Value.Action;
			}
		}

		/// <summary>
		/// Gets the interval in which the action is run.
		/// </summary>
		public int Interval
		{
			get
			{
				return Value.Interval;
			}
		}
	}
}
