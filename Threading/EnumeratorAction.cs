﻿using System;

namespace Pfz.Threading
{
	/// <summary>
	/// Delegate type to create enumerators based on actions.
	/// </summary>
	public delegate void EnumeratorAction<T>(Action<T> yieldReturn);
}
