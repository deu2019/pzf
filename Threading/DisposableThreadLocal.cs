﻿using System;

namespace Pfz.Threading
{
	/// <summary>
	/// This is an equivalent to ThreadLocal class, but it calls Dispose on it items when it is disposed, or when its threads dies.
	/// </summary>
	public sealed class DisposableThreadLocal:
		BaseDisposableThreadLocal
	{
		/// <summary>
		/// Gets the value for the actual thread, creating it if needed.
		/// </summary>
		public IDisposable GetOrCreateValue(Func<IDisposable> valueFactory)
		{
			return base._GetOrCreateValue(valueFactory);
		}

		/// <summary>
		/// Gets or sets the value for the actual thread.
		/// </summary>
		public IDisposable Value
		{
			get
			{
				return base._Value;
			}
			set
			{
				base._Value = value;
			}
		}
	}

	/// <summary>
	/// A typed version of DisposableThreadLocal.
	/// </summary>
	public sealed class DisposableThreadLocal<T>:
		BaseDisposableThreadLocal
	where
		T: class, IDisposable
	{
		/// <summary>
		/// Creates a new instance of DisposableThreadLocal.
		/// </summary>
		public DisposableThreadLocal():
			base()
		{
		}

		/// <summary>
		/// Gets the value for the actual thread, creating it if needed.
		/// </summary>
		public T GetOrCreateValue(Func<IDisposable> valueFactory)
		{
			return (T)base._GetOrCreateValue(valueFactory);
		}

		/// <summary>
		/// Gets or sets the value for the actual thread.
		/// </summary>
		public IDisposable Value
		{
			get
			{
				return (T)base._Value;
			}
			set
			{
				base._Value = value;
			}
		}
	}
}
