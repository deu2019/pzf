﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;

namespace Pfz.Threading
{
    /// <summary>
    /// Argument passed to SafeAbort.Verifying when AbortIfSafe is still unsure if it is safe
    /// to abort the thread and is able to run user validations.
    /// </summary>
    public class SafeAbortEventArgs:
        EventArgs
    {
        internal SafeAbortEventArgs(Thread thread, StackTrace stackTrace, StackFrame[] stackFrames)
        {
            Thread = thread;
            StackTrace = stackTrace;
            StackFrames = new ReadOnlyCollection<StackFrame>(stackFrames);
            CanAbort = true;
        }

        /// <summary>
        /// Gets the Thread that is about to be aborted.
        /// </summary>
        public Thread Thread { get; private set;  }

        /// <summary>
        /// Gets the StackTrace of the thread about to be aborted.
        /// </summary>
        public StackTrace StackTrace { get; private set; }

        /// <summary>
        /// Get the StackFrames of the thread to be aborted.
        /// </summary>
        public ReadOnlyCollection<StackFrame> StackFrames { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating if the thread can be aborted.
        /// </summary>
        public bool CanAbort { get; set; }
    }
}
