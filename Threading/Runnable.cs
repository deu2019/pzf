﻿
namespace Pfz.Threading
{
	internal sealed class Runnable<T>:
		IRunnable
	{
		private IRunnable<T> _runnable;
		private T _value;

		internal Runnable(IRunnable<T> runnable, T value)
		{
			_runnable = runnable;
			_value = value;
		}

		public void Run()
		{
			_runnable.Run(_value);
		}
	}
}
