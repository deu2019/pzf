﻿using System;
using System.Threading;

namespace Pfz.Threading
{
	/// <summary>
	/// This class only configures what to do when a dead-lock occurs while
	/// acquiring a lock with PfzMonitorLockExtensions or with 
	/// PfzReaderWriterLockExtensions.
	/// </summary>
	public static class LockConfiguration
	{
		#region DefaultLockTimeout
			private static int _defaultLockTimeout = 60 * 1000;
			
			/// <summary>
			/// Gets or sets the default timeout for LockWithTimeout method.
			/// This value is in milliseconds.
			/// </summary>
			public static int DefaultLockTimeout
			{
				get
				{
					return _defaultLockTimeout;
				}
				set
				{
					if (value < 0)
						throw new ArgumentException("DefaultLockTimeout must be greater or equal to zero.");
						
					_defaultLockTimeout = value;
				}
			}
		#endregion
		
		#region ThrowLockDisposerException
			internal static void _ThrowLockDisposerException(string lockName)
			{
				throw new SynchronizationLockException(lockName + " not released error. This could be caused because you forgot to call Dispose() on the lock holder or by a Thread.Abort(). If it is the second case, consider using the " + lockName + "() that accepts an action, which is abort-safe.");
			}
		#endregion
	}
}
