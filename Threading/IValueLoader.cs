﻿
namespace Pfz.Threading
{
	/// <summary>
	/// Interface implemented by BackgroundLoader and by LazyAndWeak, so you can
	/// change one for the other at run-time.
	/// </summary>
	public interface IValueLoader<out T>:
		IAdvancedDisposable
	where
		T: class
	{
		/// <summary>
		/// Gets the Value. Creates it if needed.
		/// Will only return null if this object was disposed.
		/// </summary>
		T Value { get; }

		/// <summary>
		/// Gets the Value only if it is already in memory.
		/// So, may return null if the object was disposed, if the value was
		/// never loaded or if the value was collected.
		/// </summary>
		T InMemoryValue { get; }
	}
}
