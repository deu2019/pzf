﻿using System;
using Pfz.Threading.Unsafe;

namespace Pfz.Threading
{
	/// <summary>
	/// Class that creates a Timer that runs an action at given intervals.
	/// It is high precision (and cpu intensive). It tries to compensate slowdowns, but never stacks.
	/// </summary>
	public sealed class HighPrecisionTimer:
		DisposeAssurerBase<UnsafeHighPrecisionTimer>
	{
		/// <summary>
		/// Creates a new timer that will run the given action at the given interval milliseconds.
		/// If exclusiveThreadName is not null, then a new Thread will be created. Otherwise, a 
		/// UnlimitedThreadPool thread will be used.
		/// </summary>
		public HighPrecisionTimer(Action action, double interval, string exclusiveThreadName=null):
			base(new UnsafeHighPrecisionTimer(action, interval, exclusiveThreadName))
		{
		}

		/// <summary>
		/// Creates a new timer that will run the given action at the given interval milliseconds.
		/// If exclusiveThreadName is not null, then a new Thread will be created. Otherwise, a 
		/// UnlimitedThreadPool thread will be used.
		/// </summary>
		public HighPrecisionTimer(Action action, TimeSpan interval, string exclusiveThreadName=null):
			base(new UnsafeHighPrecisionTimer(action, interval, exclusiveThreadName))
		{
		}

		/// <summary>
		/// Gets the action that's run by this timer.
		/// </summary>
		public Action Action
		{
			get
			{
				return Value.Action;
			}
		}

		/// <summary>
		/// Gets or sets the interval in which the action is run.
		/// </summary>
		public TimeSpan Interval
		{
			get
			{
				return Value.Interval;
			}
            set
            {
                Value.Interval = value;
            }
		}
	}
}
