﻿
namespace Pfz.Threading
{
	/// <summary>
	/// Interface used by objects capable of "Running".
	/// In threading scenarios, it is usually better to run a Runnable, with is rightly typed in its execution
	/// and must have all fields/properties set than to use Actions and possible unnamed delegates, which
	/// can use volatile data and, for threads, may be at corrupt states.
	/// </summary>
	public interface IRunnable
	{
		/// <summary>
		/// Runs this object.
		/// </summary>
		void Run();
	}

	/// <summary>
	/// IRunnable interface that accepts a parameter.
	/// </summary>
	public interface IRunnable<in T>
	{
		/// <summary>
		/// Runs this object.
		/// </summary>
		void Run(T value);
	}
}
