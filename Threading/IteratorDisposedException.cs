﻿using System;

namespace Pfz.Threading
{
	/// <summary>
	/// Exception thrown by the threaded iterator when the iterator is disposed.
	/// If you catch this exception, always rethrow it!
	/// </summary>
	[Serializable]
	public sealed class IteratorDisposedException:
		ObjectDisposedException
	{
		internal IteratorDisposedException(string objectName):
			base(objectName)
		{
		}
	}
}
