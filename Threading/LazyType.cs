﻿
namespace Pfz.Threading
{
	/// <summary>
	/// Enumeration used to define the type of the lazy
	/// loader to use by the LazyLoaderGenerator.
	/// </summary>
	public enum LazyType
	{
		/// <summary>
		/// Uses a LazyAndWeak class, so objects are only
		/// created when needed and can be collected later.
		/// </summary>
		LazyAndWeak,
		
		/// <summary>
		/// Uses the default .Net lazy class, which will
		/// load values when needed but will never unload
		/// them.
		/// </summary>
		Lazy,
		
		/// <summary>
		/// Uses the background loader class, which tries
		/// to pre-load all values in the background and also
		/// load them immediately if the value is requested.
		/// </summary>
		BackgroundLoader
	}
}
