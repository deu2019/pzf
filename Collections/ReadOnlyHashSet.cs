﻿using System;
using System.Collections.Generic;
using Pfz.DynamicObjects;

namespace Pfz.Collections
{
	/// <summary>
	/// Class responsible for creating read-only wrappers of hashsets.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public static class ReadOnlyHashSet<T>
	{
		private static readonly Func<HashSet<T>, IReadOnlyHashSet<T>> _creator;

		/// <summary>
		/// Initializes the dynamic-type.
		/// </summary>
		static ReadOnlyHashSet()
		{
			var adapterGenerator = new AdapterGenerator(null);
			_creator = adapterGenerator.GetCreator<HashSet<T>, IReadOnlyHashSet<T>>();
		}

		/// <summary>
		/// Creates a read-only wrapper over a hashset.
		/// </summary>
		public static IReadOnlyHashSet<T> Create(HashSet<T> modifiableHashset)
		{
			if (modifiableHashset == null)
				throw new ArgumentNullException("modifiableHashset");

			return _creator(modifiableHashset);
		}
	}
}
