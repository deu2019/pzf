﻿using System;

namespace Pfz.Collections
{
	/// <summary>
	/// Interface that must be implemented by classes that may generate fast enumerators.
	/// </summary>
	public interface IFastEnumerable<out T>:
		IAdvancedDisposable
	where
		T: class
	{
		/// <summary>
		/// Gets a fast enumerator (null means enumeration ending).
		/// </summary>
		IFastEnumerator<T> GetEnumerator();
	}
}
