﻿using System;

namespace Pfz
{
	/// <summary>
	/// Class similar to Buffer, but has its unsafe methods available to anyone.
	/// </summary>
	[CLSCompliant(false)]
	public static unsafe class UnsafeBuffer
	{
		/// <summary>
		/// Copies bytes from a source to a destination.
		/// </summary>
		public static void BlockCopy(byte *source, byte *destination, int count)
		{
			var longSource = (long *)source;
			var longDestination = (long *)destination;

			int longCount = count / sizeof(long);
			for (int i = 0; i < longCount; i++)
				*longDestination++ = *longSource++;

			source = (byte *)longSource;
			destination = (byte *)longDestination;
			int mod = count % sizeof(long);
			for (int i = 0; i < mod; i++)
				*destination++ = *source++;
		}
	}
}
