﻿using System;

namespace Pfz.Remoting
{
	/// <summary>
	/// Allows changing if the actual thread allows sync remoting or if the default must be used.
	/// </summary>
	public sealed class AllowSyncRemoting:
		IDisposable
	{
		#region Static Area
			[ThreadStatic]
			private static bool? _value;

			/// <summary>
			/// Gets or sets a value indicating if the actual thread allows synchronous remoting.
			/// A null value means the AllowSyncCalls from the RemotingClient will be used.
			/// You may prefer instancing this class if you want the change to be reversible.
			/// </summary>
			public static bool? Value
			{
				get
				{
					return _value;
				}
				set
				{
					_value = value;
				}
			}
		#endregion

		private bool? _oldValue;
		/// <summary>
		/// Changes the value of AllowSyncRemoting. Use it in a using clause, so it is reversible.
		/// </summary>
		public AllowSyncRemoting(bool? value)
		{
			_oldValue = _value;
			_value = value;
		}

		/// <summary>
		/// Restores the old value.
		/// </summary>
		public void Dispose()
		{
			_value = _oldValue;
		}
	}
}
