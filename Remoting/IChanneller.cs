﻿using System;

namespace Pfz.Remoting
{
	/// <summary>
	/// Interface that must be implemented by channellers. That is, thay allow many channels to be created in a single "connection".
	/// </summary>
	public interface IChanneller:
		IAdvancedDisposable
	{
		/// <summary>
		/// Gets the LocalEndPoint.
		/// </summary>
		string LocalEndpoint { get; }

		/// <summary>
		/// Gets the RemoteEndPoint.
		/// </summary>
		string RemoteEndpoint { get; }

		/// <summary>
		/// Creates a new channel.
		/// </summary>
		IChannel CreateChannel(object userData = null);

		/// <summary>
		/// Starts this channeller.
		/// </summary>
		void Start();

		/// <summary>
		/// Event invoked when a channel is created from a remote's side request.
		/// </summary>
		event EventHandler<ChannelCreatedEventArgs> ChannelCreated;
	}
}
