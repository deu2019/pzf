﻿using Pfz.Extensions;
using Pfz.Remoting.Instructions;
using Pfz.Serialization;

namespace Pfz.Remoting.Serializers
{
	internal class InvokeStaticMethodSerializer:
		ItemSerializer<InstructionInvokeStaticMethod>
	{
		internal static readonly InvokeStaticMethodSerializer Instance = new InvokeStaticMethodSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, InstructionInvokeStaticMethod item)
		{
			serializer.Stream.WriteString(item.MethodName);
			serializer.InnerSerialize(item.Parameters);
		}

		public override InstructionInvokeStaticMethod Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new InstructionInvokeStaticMethod();
			result.MethodName = deserializer.Stream.ReadString();
			result.Parameters = (object[])deserializer.InnerDeserialize();
			return result;
		}
	}
}
