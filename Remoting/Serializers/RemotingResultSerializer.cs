﻿using System;
using Pfz.Remoting.Instructions;
using Pfz.Serialization;

namespace Pfz.Remoting.Serializers
{
	internal sealed class RemotingResultSerializer:
		ItemSerializer<RemotingResult>
	{
		internal static readonly RemotingResultSerializer Instance = new RemotingResultSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, RemotingResult item)
		{
			serializer.InnerSerialize(item.Exception);
			serializer.InnerSerialize(item.OutValues);
			serializer.InnerSerialize(item.Value);
		}

		public override RemotingResult Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new RemotingResult();
			result.Exception = (Exception)deserializer.InnerDeserialize();
			result.OutValues = (object[])deserializer.InnerDeserialize();
			result.Value = deserializer.InnerDeserialize();
			return result;
		}
	}
}
