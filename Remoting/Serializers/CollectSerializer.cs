﻿using Pfz.Remoting.Instructions;
using Pfz.Serialization;

namespace Pfz.Remoting.Serializers
{
	internal sealed class CollectSerializer:
		ItemSerializer<InstructionCollect>
	{
		internal static readonly CollectSerializer Instance = new CollectSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, InstructionCollect item)
		{
		}

		public override InstructionCollect Deserialize(ConfigurableSerializerBase deserializer)
		{
			return InstructionCollect.Instance;
		}
	}
}
