﻿using Pfz.Extensions;
using Pfz.Remoting.Internal;
using Pfz.Serialization;

namespace Pfz.Remoting.Serializers
{
	internal sealed class _BackObjectReferenceSerializer:
		ItemSerializer<_BackObjectReference>
	{
		internal static readonly _BackObjectReferenceSerializer Instance = new _BackObjectReferenceSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, _BackObjectReference item)
		{
			serializer.Stream.WriteCompressedInt64(item.Id);
		}

		public override _BackObjectReference Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new _BackObjectReference();
			result.Id = deserializer.Stream.ReadCompressedInt64();
			return result;
		}
	}
}
