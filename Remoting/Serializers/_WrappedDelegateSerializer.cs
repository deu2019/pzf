﻿using Pfz.Extensions;
using Pfz.Remoting.Internal;
using Pfz.Serialization;
using Pfz.Serialization.BinaryBuiltIn;

namespace Pfz.Remoting.Serializers
{
	internal sealed class _WrappedDelegateSerializer:
		ItemSerializer<_WrappedDelegate>
	{
		internal static readonly _WrappedDelegateSerializer Instance = new _WrappedDelegateSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, _WrappedDelegate item)
		{
			TypeSerializer.Instance.Serialize(serializer, item.DelegateType);
			serializer.Stream.WriteCompressedInt64(item.Id);
		}

		public override _WrappedDelegate Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new _WrappedDelegate();
			result.DelegateType = TypeSerializer.Instance.Deserialize(deserializer);
			result.Id = deserializer.Stream.ReadCompressedInt64();
			return result;
		}
	}
}
