﻿using System;
using Pfz.Extensions;
using Pfz.Remoting.Instructions;
using Pfz.Serialization;
using Pfz.Serialization.BinaryBuiltIn;

namespace Pfz.Remoting.Serializers
{
	internal sealed class InvokeMethodSerializer:
		ItemSerializer<InstructionInvokeMethod>
	{
		internal static readonly InvokeMethodSerializer Instance = new InvokeMethodSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, InstructionInvokeMethod item)
		{
			serializer.InnerSerialize(item.GenericArguments);
			MethodInfoSerializer.Instance.Serialize(serializer, item.MethodInfo);
			serializer.Stream.WriteCompressedInt64(item.ObjectId);
			serializer.InnerSerialize(item.Parameters);
		}

		public override InstructionInvokeMethod Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new InstructionInvokeMethod();
			result.GenericArguments = (Type[])deserializer.InnerDeserialize();
			result.MethodInfo = MethodInfoSerializer.Instance.Deserialize(deserializer);
			result.ObjectId = deserializer.Stream.ReadCompressedInt64();
			result.Parameters = (object[])deserializer.InnerDeserialize();
			return result;
		}
	}
}
