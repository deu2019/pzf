﻿using Pfz.Extensions;
using Pfz.Remoting.Instructions;
using Pfz.Serialization;

namespace Pfz.Remoting.Serializers
{
	internal sealed class InvokeDelegateSerializer:
		ItemSerializer<InstructionInvokeDelegate>
	{
		internal static readonly InvokeDelegateSerializer Instance = new InvokeDelegateSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, InstructionInvokeDelegate item)
		{
			serializer.Stream.WriteCompressedInt64(item.HandlerId);
			serializer.InnerSerialize(item.Parameters);
		}

		public override InstructionInvokeDelegate Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new InstructionInvokeDelegate();
			result.HandlerId = deserializer.Stream.ReadCompressedInt64();
			result.Parameters = (object[])deserializer.InnerDeserialize();
			return result;
		}
	}
}
