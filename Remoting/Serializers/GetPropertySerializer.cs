﻿using Pfz.Extensions;
using Pfz.Remoting.Instructions;
using Pfz.Serialization;
using Pfz.Serialization.BinaryBuiltIn;

namespace Pfz.Remoting.Serializers
{
	internal sealed class GetPropertySerializer:
		ItemSerializer<InstructionGetProperty>
	{
		internal static readonly GetPropertySerializer Instance = new GetPropertySerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, InstructionGetProperty item)
		{
			serializer.Stream.WriteCompressedInt64(item.ObjectId);
			PropertyInfoSerializer.Instance.Serialize(serializer, item.PropertyInfo);
		}

		public override InstructionGetProperty Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new InstructionGetProperty();
			result.ObjectId = deserializer.Stream.ReadCompressedInt64();
			result.PropertyInfo = PropertyInfoSerializer.Instance.Deserialize(deserializer);
			return result;
		}
	}
}
