﻿using Pfz.Extensions;
using Pfz.Remoting.Internal;
using Pfz.Serialization;

namespace Pfz.Remoting.Serializers
{
	internal sealed class _BackDelegateReferenceSerializer:
		ItemSerializer<_BackDelegateReference>
	{
		internal static readonly _BackDelegateReferenceSerializer Instance = new _BackDelegateReferenceSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, _BackDelegateReference item)
		{
			serializer.Stream.WriteCompressedInt64(item.Id);
		}

		public override _BackDelegateReference Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new _BackDelegateReference();
			result.Id = deserializer.Stream.ReadCompressedInt64();
			return result;
		}
	}
}
