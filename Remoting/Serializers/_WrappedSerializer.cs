﻿using System;
using Pfz.Extensions;
using Pfz.Remoting.Internal;
using Pfz.Serialization;

namespace Pfz.Remoting.Serializers
{
	internal sealed class _WrappedSerializer:
		ItemSerializer<_Wrapped>
	{
		public static readonly _WrappedSerializer Instance = new _WrappedSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, _Wrapped item)
		{
			serializer.Stream.WriteCompressedInt64(item.Id);
			serializer.InnerSerialize(item.InterfaceTypes);
		}

		public override _Wrapped Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new _Wrapped();
			result.Id = deserializer.Stream.ReadCompressedInt64();
			result.InterfaceTypes = (Type[])deserializer.InnerDeserialize();
			return result;
		}
	}
}
