﻿using Pfz.Remoting.Internal;
using Pfz.Serialization;

namespace Pfz.Remoting.Serializers
{
	internal sealed class _FakeNullSerializer:
		ItemSerializer<_FakeNull>
	{
		public static readonly _FakeNullSerializer Instance = new _FakeNullSerializer();

		private _FakeNullSerializer()
		{
		}

		public override void Serialize(ConfigurableSerializerBase serializer, _FakeNull item)
		{
		}

		public override _FakeNull Deserialize(ConfigurableSerializerBase deserializer)
		{
			return _FakeNull.Instance;
		}
	}
}
