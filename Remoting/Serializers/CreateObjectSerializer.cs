﻿using Pfz.Extensions;
using Pfz.Remoting.Instructions;
using Pfz.Serialization;

namespace Pfz.Remoting.Serializers
{
	internal sealed class CreateObjectSerializer:
		ItemSerializer<InstructionCreateObject>
	{
		internal static readonly CreateObjectSerializer Instance = new CreateObjectSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, InstructionCreateObject item)
		{
			serializer.Stream.WriteString(item.Name);
		}

		public override InstructionCreateObject Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new InstructionCreateObject();
			result.Name = deserializer.Stream.ReadString();
			return result;
		}
	}
}
