﻿using System;
using Pfz.Extensions;
using Pfz.Remoting.Instructions;
using Pfz.Serialization;
using Pfz.Serialization.BinaryBuiltIn;

namespace Pfz.Remoting.Serializers
{
	internal sealed class AddEventSerializer:
		ItemSerializer<InstructionAddEvent>
	{
		internal static readonly AddEventSerializer Instance = new AddEventSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, InstructionAddEvent item)
		{
			EventInfoSerializer.Instance.Serialize(serializer, item.EventInfo);
			serializer.InnerSerialize(item.Handler);
			serializer.Stream.WriteCompressedInt64(item.ObjectId);
		}

		public override InstructionAddEvent Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new InstructionAddEvent();
			result.EventInfo = EventInfoSerializer.Instance.Deserialize(deserializer);
			result.Handler = (Delegate)deserializer.InnerDeserialize();
			result.ObjectId = deserializer.Stream.ReadCompressedInt64();
			return result;
		}
	}
}
