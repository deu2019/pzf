﻿using Pfz.Extensions;
using Pfz.Remoting.Instructions;
using Pfz.Serialization;
using Pfz.Serialization.BinaryBuiltIn;

namespace Pfz.Remoting.Serializers
{
	internal sealed class SetPropertySerializer:
		ItemSerializer<InstructionSetProperty>
	{
		internal static readonly SetPropertySerializer Instance = new SetPropertySerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, InstructionSetProperty item)
		{
			serializer.Stream.WriteCompressedInt64(item.ObjectId);
			PropertyInfoSerializer.Instance.Serialize(serializer, item.PropertyInfo);
			serializer.InnerSerialize(item.Value);
		}

		public override InstructionSetProperty Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new InstructionSetProperty();
			result.ObjectId = deserializer.Stream.ReadCompressedInt64();
			result.PropertyInfo = PropertyInfoSerializer.Instance.Deserialize(deserializer);
			result.Value = deserializer.InnerDeserialize();
			return result;
		}
	}
}
