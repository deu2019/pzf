﻿using System;
using Pfz.Extensions;
using Pfz.Remoting.Instructions;
using Pfz.Serialization;
using Pfz.Serialization.BinaryBuiltIn;

namespace Pfz.Remoting.Serializers
{
	internal sealed class RemoveEventSerializer:
		ItemSerializer<InstructionRemoveEvent>
	{
		internal static readonly RemoveEventSerializer Instance = new RemoveEventSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, InstructionRemoveEvent item)
		{
			EventInfoSerializer.Instance.Serialize(serializer, item.EventInfo);
			serializer.InnerSerialize(item.Handler);
			serializer.Stream.WriteCompressedInt64(item.ObjectId);
		}

		public override InstructionRemoveEvent Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new InstructionRemoveEvent();
			result.EventInfo = EventInfoSerializer.Instance.Deserialize(deserializer);
			result.Handler = (Delegate)deserializer.InnerDeserialize();
			result.ObjectId = deserializer.Stream.ReadCompressedInt64();
			return result;
		}
	}
}
