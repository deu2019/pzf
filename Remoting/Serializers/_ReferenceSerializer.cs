﻿using Pfz.Extensions;
using Pfz.Remoting.Internal;
using Pfz.Serialization;

namespace Pfz.Remoting.Serializers
{
	internal sealed class _ReferenceSerializer:
		ItemSerializer<_Reference>
	{
		internal static readonly _ReferenceSerializer Instance = new _ReferenceSerializer();

		public override void Serialize(ConfigurableSerializerBase serializer, _Reference item)
		{
			serializer.Stream.WriteCompressedInt64(item.Id);
		}

		public override _Reference Deserialize(ConfigurableSerializerBase deserializer)
		{
			var result = new _Reference();
			result.Id = deserializer.Stream.ReadCompressedInt64();
			return result;
		}
	}
}
