﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pfz.Remoting.Internal
{
	internal sealed class _FakeNull
	{
		public static readonly _FakeNull Instance = new _FakeNull();

		private _FakeNull()
		{
		}
	}
}
