﻿using System;
using Pfz.Serialization;

namespace Pfz.Remoting.Internal
{
	//[Serializable]
	internal sealed class _WrappedDelegate:
		_ReferenceOrWrapped
	{
		public Type DelegateType;

		public override object GetResult(RemotingClient remotingClient, RemotingSerializer serializer)
		{
			serializer._wrapCount++;
			var result = remotingClient._GetWrappedDelegate(this);
			return result;
		}
	}
}
