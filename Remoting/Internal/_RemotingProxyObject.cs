﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Pfz.DynamicObjects;
using Pfz.Remoting.Instructions;

namespace Pfz.Remoting.Internal
{
	internal sealed class _RemotingProxyObject:
		_RemotingProxy,
		IProxyObject
	{
		internal _RemotingProxyObject(RemotingClient client, _Wrapped wrapped)
		{
			RemotingClient = client;
			Id = wrapped.Id;
		}

		public object ImplementedObject { get; internal set; }

		[SuppressMessage("Microsoft.Usage", "CA2219:DoNotRaiseExceptionsInExceptionClauses")]
		public object InvokeMethod(MethodInfo methodInfo, Type[] genericArguments, object[] parameters)
		{
			InvokeMethod_EventArgs args = null;
			EventHandler<InvokeMethod_EventArgs> before = null;
			EventHandler<InvokeMethod_EventArgs> after = null;
			var events = RemotingClient._parameters._invokeMethodEvents;

			if (events != null)
			{
				before = events._beforeRedirect;
				after = events._afterRedirect;

				if (before != null || after != null)
				{
					args = new InvokeMethod_EventArgs();
					args.GenericArguments = genericArguments;
					args.MethodInfo = methodInfo;
					args.Parameters = parameters;
					args.Target = ImplementedObject;
				}
			}

			object result = null;
			try
			{
				if (before != null)
				{
					before(RemotingClient, args);
					if (!args.CanInvoke)
						return args.Result;
				}

				var instruction = new InstructionInvokeMethod();
				instruction.GenericArguments = genericArguments;
				instruction.MethodInfo = methodInfo;
				instruction.ObjectId = Id;
				instruction.Parameters = parameters;

				return RemotingClient._Invoke(instruction, methodInfo, parameters);
			}
			catch(RemotingSyncException)
			{
				throw;
			}
			catch(Exception exception)
			{
				if (after == null || !args.CanInvoke)
					throw;

				args.Exception = exception;
			}
			finally
			{
				if (args != null)
				{
					if (after != null && args.CanInvoke)
					{
						args.Result = result;
						after(RemotingClient, args);
						result = args.Result;
					}

					var ex = args.Exception;
					if (ex != null)
						throw ex;
				}
			}

			return result;
		}

		[SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0"), SuppressMessage("Microsoft.Usage", "CA2219:DoNotRaiseExceptionsInExceptionClauses")]
		public object InvokePropertyGet(PropertyInfo propertyInfo)
		{
			InvokeProperty_EventArgs args = null;
			EventHandler<InvokeProperty_EventArgs> before = null;
			EventHandler<InvokeProperty_EventArgs> after = null;
			var events = RemotingClient._parameters._invokePropertyGetEvents;

			if (events != null)
			{
				before = events._beforeRedirect;
				after = events._afterRedirect;

				if (before != null || after != null)
				{
					args = new InvokeProperty_EventArgs();
					args.PropertyInfo = propertyInfo;
					args.Target = ImplementedObject;
				}
			}

			object result = null;
			try
			{
				if (before != null)
				{
					before(RemotingClient, args);
					if (!args.CanInvoke)
						return args.Value;
				}

				var instruction = new InstructionGetProperty();
				instruction.ObjectId = Id;
				instruction.PropertyInfo = propertyInfo;

				result = RemotingClient._Invoke(instruction);
			}
			catch(RemotingSyncException)
			{
				throw;
			}
			catch(Exception exception)
			{
				if (after == null || !args.CanInvoke)
					throw;

				args.Exception = exception;
			}
			finally
			{
				if (args != null)
				{
					if (after != null && args.CanInvoke)
					{
						args.Value = result;
						after(RemotingClient, args);
						result = args.Value;
					}

					var ex = args.Exception;
					if (ex != null)
						throw ex;
				}
			}

			return result;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0"), SuppressMessage("Microsoft.Usage", "CA2219:DoNotRaiseExceptionsInExceptionClauses")]
		public void InvokePropertySet(PropertyInfo propertyInfo, object value)
		{
			InvokeProperty_EventArgs args = null;
			EventHandler<InvokeProperty_EventArgs> before = null;
			EventHandler<InvokeProperty_EventArgs> after = null;
			var events = RemotingClient._parameters._invokePropertySetEvents;

			if (events != null)
			{
				before = events._beforeRedirect;
				after = events._afterRedirect;

				if (before != null || after != null)
				{
					args = new InvokeProperty_EventArgs();
					args.PropertyInfo = propertyInfo;
					args.Target = ImplementedObject;
					args.Value = value;
				}
			}

			try
			{
				if (before != null)
				{
					before(RemotingClient, args);
					if (!args.CanInvoke)
						return;
				}

				var instruction = new InstructionSetProperty();
				instruction.ObjectId = Id;
				instruction.PropertyInfo = propertyInfo;
				instruction.Value = value;

				RemotingClient._Invoke(instruction);
			}
			catch(RemotingSyncException)
			{
				throw;
			}
			catch(Exception exception)
			{
				if (after == null)
					throw;

				args.Exception = exception;
			}
			finally
			{
				if (args != null)
				{
					if (after != null && args.CanInvoke)
						after(RemotingClient, args);

					var ex = args.Exception;
					if (ex != null)
						throw ex;
				}
			}
		}

		[SuppressMessage("Microsoft.Usage", "CA2219:DoNotRaiseExceptionsInExceptionClauses")]
		public void InvokeEventAdd(EventInfo eventInfo, Delegate handler)
		{
			InvokeEvent_EventArgs args = null;
			EventHandler<InvokeEvent_EventArgs> before = null;
			EventHandler<InvokeEvent_EventArgs> after = null;
			var events = RemotingClient._parameters._invokeEventAddEvents;

			if (events != null)
			{
				before = events._beforeRedirect;
				after = events._afterRedirect;

				if (before != null || after != null)
				{
					args = new InvokeEvent_EventArgs();
					args.EventInfo = eventInfo;
					args.Handler = handler;
					args.Target = ImplementedObject;
				}
			}

			try
			{
				if (before != null)
				{
					before(RemotingClient, args);
					if (!args.CanInvoke)
						return;
				}

				var instruction = new InstructionAddEvent();
				instruction.EventInfo = eventInfo;
				instruction.Handler = handler;
				instruction.ObjectId = Id;

				RemotingClient._Invoke(instruction);
			}
			catch(RemotingSyncException)
			{
				throw;
			}
			catch(Exception exception)
			{
				if (after == null || !args.CanInvoke)
					throw;

				args.Exception = exception;
			}
			finally
			{
				if (args != null)
				{
					if (after != null && args.CanInvoke)
						after(RemotingClient, args);

					var ex = args.Exception;
					if (ex != null)
						throw ex;
				}
			}
		}

		[SuppressMessage("Microsoft.Usage", "CA2219:DoNotRaiseExceptionsInExceptionClauses")]
		public void InvokeEventRemove(EventInfo eventInfo, Delegate handler)
		{
			InvokeEvent_EventArgs args = null;
			EventHandler<InvokeEvent_EventArgs> before = null;
			EventHandler<InvokeEvent_EventArgs> after = null;
			var events = RemotingClient._parameters._invokeEventRemoveEvents;

			if (events != null)
			{
				before = events._beforeRedirect;
				after = events._afterRedirect;

				if (before != null || after != null)
				{
					args = new InvokeEvent_EventArgs();
					args.EventInfo = eventInfo;
					args.Handler = handler;
					args.Target = ImplementedObject;
				}
			}

			try
			{
				if (before != null)
				{
					before(RemotingClient, args);
					if (!args.CanInvoke)
						return;
				}

				var instruction = new InstructionRemoveEvent();
				instruction.EventInfo = eventInfo;
				instruction.Handler = handler;
				instruction.ObjectId = Id;

				RemotingClient._Invoke(instruction);
			}
			catch(RemotingSyncException)
			{
				throw;
			}
			catch(Exception exception)
			{
				if (after == null || !args.CanInvoke)
					throw;

				args.Exception = exception;
			}
			finally
			{
				if (args != null)
				{
					if (after != null && args.CanInvoke)
						after(RemotingClient, args);

					var ex = args.Exception;
					if (ex != null)
						throw ex;
				}
			}
		}

		public override _ReferenceOrWrapped GetBackReference()
		{
			return new _BackObjectReference { Id = this.Id };
		}
	}
}
