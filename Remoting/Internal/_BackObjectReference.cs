﻿using System;
using Pfz.Serialization;

namespace Pfz.Remoting.Internal
{
	//[Serializable]
	internal sealed class _BackObjectReference:
		_ReferenceOrWrapped
	{
		public override object GetResult(RemotingClient remotingClient, RemotingSerializer serializer)
		{
			var result = remotingClient._objectsUsedByTheOtherSide.Get(Id);
			return result;
		}
	}
}
