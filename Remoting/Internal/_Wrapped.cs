﻿using System;
using Pfz.Serialization;

namespace Pfz.Remoting.Internal
{
	//[Serializable]
	internal sealed class _Wrapped:
		_ReferenceOrWrapped
	{
		public Type[] InterfaceTypes;

		public override object GetResult(RemotingClient remotingClient, RemotingSerializer serializer)
		{
			serializer._wrapCount++;
			var result = remotingClient._GetWrappedObject(this);
			return result;
		}
	}
}
