﻿using System;
using Pfz.Serialization;

namespace Pfz.Remoting.Internal
{
	//[Serializable]
	internal abstract class _ReferenceOrWrapped
	{
		public long Id;

		public abstract object GetResult(RemotingClient remotingClient, RemotingSerializer serializer);
	}
}
