﻿using System;
using Pfz.Serialization;

namespace Pfz.Remoting.Internal
{
	internal sealed class _BackDelegateReference:
		_ReferenceOrWrapped
	{
		public override object GetResult(RemotingClient remotingClient, RemotingSerializer serializer)
		{
			var result = remotingClient._objectsUsedByTheOtherSide.Get(Id);
			return result;
		}
	}
}
