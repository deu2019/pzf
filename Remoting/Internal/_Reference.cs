﻿using System;
using Pfz.Serialization;

namespace Pfz.Remoting.Internal
{
	//[Serializable]
	internal sealed class _Reference:
		_ReferenceOrWrapped
	{
		public override object GetResult(RemotingClient remotingClient, RemotingSerializer serializer)
		{
			var result = remotingClient._GetReferencedObject(this);
			return result;
		}
	}
}
