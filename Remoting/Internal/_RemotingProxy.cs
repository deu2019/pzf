﻿using Pfz.Remoting.Instructions;

namespace Pfz.Remoting.Internal
{
	internal abstract class _RemotingProxy
	{
		internal _RemotingProxy()
		{
		}

		public RemotingClient RemotingClient { get; internal set; }
		internal long Id { get; set; }
		
		public abstract _ReferenceOrWrapped GetBackReference();
	}
}
