﻿using Pfz.Threading;

namespace Pfz.Remoting.Internal
{
	internal sealed class _RemotingSetter:
		IRunnable
	{
		internal _RemotingSetter(RemotingClient client)
		{
			_client = client;
		}

		private readonly RemotingClient _client;
		public void Run()
		{
			RemotingClient._executingClient = _client;
		}
	}
}
