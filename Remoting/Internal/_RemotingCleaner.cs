﻿using Pfz.Threading;

namespace Pfz.Remoting.Internal
{
	internal sealed class _RemotingCleaner:
		IRunnable
	{
		internal static readonly _RemotingCleaner Instance = new _RemotingCleaner();

		public void Run()
		{
			RemotingClient._executingClient = null;
		}
	}
}
