﻿using System.Net.Sockets;

namespace Pfz.Remoting
{
	/// <summary>
	/// This is a channeller capable of connecting using MemoryMappedFiles for fast local connections, or Tcp/Ip for remote connections.
	/// </summary>
	public static class HybridChanneller
	{
		/// <summary>
		/// Creates a new channeller using the appropriate mode (MemoryMappedFiles or Tcp/Ip).
		/// </summary>
		public static IChanneller Connect(string hostname, int port, int bufferLength)
		{
			if (string.IsNullOrWhiteSpace(hostname) || hostname == "127.0.0.1" || hostname.ToLower() == "localhost")
			{
				var result = new MmfChanneller("Pfz.Remoting.HybridChanneller:" + port);
				result.ChannelBufferLength = bufferLength;
				return result;
			}

			var client = new TcpClient(hostname, port);
			var channeller = ChannellerConnection.Create(client, bufferLength);
			return channeller;
		}
	}
}
