﻿
namespace Pfz.Remoting
{
	/// <summary>
	/// Interface that must be implemented by "listeners" that accept "channellers".
	/// That is: A listener accepts a new connection. Then, many channels can be created in the same connection.
	/// </summary>
	public interface IChannellerListener:
		IAdvancedDisposable
	{
		/// <summary>
		/// Accepts a new connection or returns null if the connection is closed.
		/// May still throw exceptions for other reasons.
		/// </summary>
		IChanneller TryAccept();
	}
}
