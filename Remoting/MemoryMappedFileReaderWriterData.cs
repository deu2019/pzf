﻿
namespace Pfz.Remoting
{
	internal struct MemoryMappedFileReaderWriterData
	{
		internal volatile int _readerPosition;
		internal volatile bool _readerState;

		internal volatile int _writerPosition;
		internal volatile bool _writerState;

		internal volatile bool _ended;
	}
}
