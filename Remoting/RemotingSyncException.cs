﻿using System;
using System.Runtime.Serialization;

namespace Pfz.Remoting
{
	/// <summary>
	/// Exception thrown by a RemotingClient when trying to do a synchronous remoting call by
	/// a remoting client that does not allow such calls.
	/// </summary>
	[Serializable]
	public class RemotingSyncException:
		RemotingException
	{
		/// <summary>
		/// Pattern.
		/// </summary>
		public RemotingSyncException()
		{
		}

		/// <summary>
		/// Pattern.
		/// </summary>
		public RemotingSyncException(string message):
			base(message)
		{
		}

		/// <summary>
		/// Pattern.
		/// </summary>
		public RemotingSyncException(string message, Exception inner):
			base(message, inner)
		{
		}

		/// <summary>
		/// Pattern.
		/// </summary>
		protected RemotingSyncException(SerializationInfo info, StreamingContext context):
			base(info, context)
		{
		}
	}
}
