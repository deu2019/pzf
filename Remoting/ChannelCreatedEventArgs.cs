﻿using System;

namespace Pfz.Remoting
{
	/// <summary>
	/// Argument passed to Channellers when a new channel is created.
	/// </summary>
	[Serializable]
	public class ChannelCreatedEventArgs:
		EventArgs
	{
		/// <summary>
		/// The created channel.
		/// </summary>
		public IChannel Channel { get; set; }

		/// <summary>
		/// The user-data provided when creating the channel.
		/// </summary>
		public object Data { get; set; }
	}
}
