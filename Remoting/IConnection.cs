﻿using System.IO;

namespace Pfz.Remoting
{
	/// <summary>
	/// Interface used to make communication on the client side more generic.
	/// </summary>
	public interface IConnection:
		IAdvancedDisposable
	{
		/// <summary>
		/// Gets the Local endpoint as string.
		/// </summary>
		string LocalEndpoint { get; }

		/// <summary>
		/// Gets the Remote endpoint as string.
		/// </summary>
		string RemoteEndpoint { get; }

		/// <summary>
		/// Gets or sets the stream used by this client.
		/// Only set the stream if you are decorating it, or you could cause serious bugs.
		/// </summary>
		Stream Stream { get; set; }
	}
}
