﻿
namespace Pfz.Remoting
{
	/// <summary>
	/// The cryptography side used by TcpChanneller and TcpChannellerListener.
	/// </summary>
	public enum CryptographySide
	{
		/// <summary>
		/// Does not use cryptography.
		/// </summary>
		None,

		/// <summary>
		/// Starts the cryptography as the server.
		/// </summary>
		Server,

		/// <summary>
		/// Starts the cryptography as the client.
		/// </summary>
		Client
	}
}
