﻿using System.IO;

namespace Pfz.Remoting
{
	/// <summary>
	/// Interface that represents a channel of communication.
	/// </summary>
	public interface IChannel:
		IAdvancedDisposable
	{
		/// <summary>
		/// Gets the channeller that created this channel.
		/// </summary>
		IChanneller Channeller { get; }

		/// <summary>
		/// Gets the LocalEndPoint.
		/// </summary>
		string LocalEndpoint { get; }

		/// <summary>
		/// Gets the RemoteEndPoint.
		/// </summary>
		string RemoteEndpoint { get; }

		/// <summary>
		/// Gets the stream used for communication.
		/// </summary>
		Stream Stream { get; }
	}
}
