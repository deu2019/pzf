﻿using System;

namespace Pfz.Remoting
{
	/// <summary>
	/// Argument used by RemotingServer.ClientConnected event.
	/// </summary>
	public sealed class RemotingClientConnectedEventArgs:
		EventArgs
	{
		/// <summary>
		/// Gets the Client that just connected.
		/// </summary>
		public RemotingClient Client { get; internal set; }
	}
}
