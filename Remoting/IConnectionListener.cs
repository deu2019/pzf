﻿
namespace Pfz.Remoting
{
	/// <summary>
	/// Interface used by generic-purpose listeners.
	/// Such listeners requires that you configure them before calling TryAccept, but
	/// have a more generic approach than TcpListener.
	/// </summary>
	public interface IConnectionListener<out T>:
		IAdvancedDisposable
	where
		T: IConnection
	{
		/// <summary>
		/// Tries to accept a new connection or returns null if this object was disposed.
		/// </summary>
		T TryAccept();

		/// <summary>
		/// Gets a copy of all active connections of this listener.
		/// </summary>
		T[] GetConnections();
	}
}
