﻿using System;
using System.Reflection;
using Pfz.Remoting.Internal;

namespace Pfz.Remoting.Instructions
{
	internal sealed class InstructionCreateObject:
		Instruction
	{
		public string Name;

		public override void Run(RemotingClient client, _ThreadData threadData)
		{
			threadData._Action
			(
				() =>
				{
					Func<object> constructor;
					#if DEBUG
						if (!client._parameters._registeredTypes.TryGetValue(Name, out constructorInfo))
							throw new RemotingException("Can't find a registered type named " + Name + ".");
					#else
						constructor = client._parameters._registeredTypes[Name];
					#endif

					return constructor();
				}
			);
		}
	}
}
