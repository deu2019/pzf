﻿using System;
using System.Reflection;
using Pfz.Remoting.Internal;
using Pfz.Extensions;

namespace Pfz.Remoting.Instructions
{
	internal sealed class InstructionInvokeStaticMethod:
		Instruction
	{
		public string MethodName;
		public object[] Parameters;

		public override void Run(RemotingClient client, _ThreadData threadData)
		{
			threadData._Action
			(
				() =>
				{
					FastMethodCallDelegate staticDelegate;
					client._parameters._registeredStaticMethods.TryGetValue(MethodName, out staticDelegate);

					if (staticDelegate == null)
						throw new RemotingException("Can't invoke static method " + MethodName + ".");

					var parameters = Parameters;
					return staticDelegate(null, parameters);
				}
			);
		}
	}
}
