﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Pfz.DynamicObjects;
using Pfz.Remoting.Internal;
using Pfz.Extensions;

namespace Pfz.Remoting.Instructions
{
	internal sealed class InstructionInvokeMethod:
		Instruction
	{
		public long ObjectId;
		public MethodInfo MethodInfo;
		public Type[] GenericArguments;
		public object[] Parameters;

		public override void Run(RemotingClient client, _ThreadData threadData)
		{
			var methodInfo = MethodInfo;
			threadData._Action
			(
				methodInfo,
				Parameters,
				() =>
				{
					var obj = client._objectsUsedByTheOtherSide.Get(ObjectId);

					var genericArguments = GenericArguments;
					if (genericArguments != null)
						methodInfo = methodInfo.MakeGenericMethod(genericArguments);

					var parameters = Parameters;

					InvokeMethod_EventArgs args = null;
					EventHandler<InvokeMethod_EventArgs> before = null;
					EventHandler<InvokeMethod_EventArgs> after = null;
					var events = client._parameters._invokeMethodEvents;

					if (events != null)
					{
						before = events._beforeInvoke;
						after = events._afterInvoke;

						if (before != null || after != null)
						{
							args = new InvokeMethod_EventArgs();
							args.GenericArguments = genericArguments;
							args.MethodInfo = methodInfo;
							args.Parameters = parameters;
							args.Target = obj;
						}
					}

					object result = null;
					try
					{
						if (before != null)
						{
							before(this, args);
							if (!args.CanInvoke)
								return args.Result;
						}

						var methodInfoDelegate = threadData._GetMethodInfoDelegate(methodInfo);
						result = methodInfoDelegate(obj, parameters);
					}
					catch(RemotingSyncException)
					{
						throw;
					}
					catch (Exception exception)
					{
						if (after == null || !args.CanInvoke)
							throw;

						args.Exception = exception;
					}
					finally
					{
						if (args != null)
						{
							if (after != null && args.CanInvoke)
							{
								args.Result = result;
								after(this, args);
								result = args.Result;
							}

							var ex = args.Exception;
							if (ex != null)
								throw ex;
						}
					}

					return result;
				}
			);
		}
	}
}
