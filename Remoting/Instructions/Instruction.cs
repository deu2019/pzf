﻿using System;
using Pfz.Remoting.Internal;

namespace Pfz.Remoting.Instructions
{
	internal abstract class Instruction
	{
		public abstract void Run(RemotingClient client, _ThreadData threadData);
	}
}
