﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Pfz.Caching;
using Pfz.DynamicObjects;
using Pfz.Remoting.Internal;

namespace Pfz.Remoting.Instructions
{
	internal sealed class InstructionRemoveEvent:
		InstructionEvent
	{
		[SuppressMessage("Microsoft.Usage", "CA2219:DoNotRaiseExceptionsInExceptionClauses")]
		public override void Run(RemotingClient client, _ThreadData threadData)
		{
			threadData._Action
			(
				() =>
				{
					var eventInfo = EventInfo;

					var obj = client._objectsUsedByTheOtherSide.Get(ObjectId);
					var handler = Handler;

					InvokeEvent_EventArgs args = null;
					EventHandler<InvokeEvent_EventArgs> before = null;
					EventHandler<InvokeEvent_EventArgs> after = null;
					var events = client._parameters._invokeEventRemoveEvents;

					if (events != null)
					{
						before = events._beforeInvoke;
						after = events._afterInvoke;

						if (before != null || after != null)
						{
							args = new InvokeEvent_EventArgs();
							args.EventInfo = eventInfo;
							args.Handler = handler;
							args.Target = handler.Target;
						}
					}

					try
					{
						if (before != null)
						{
							before(this, args);
							if (!args.CanInvoke)
								return null;
						}

						var fastEventRemove = ReflectionHelper.GetEventRemoverDelegate(eventInfo);
						fastEventRemove(obj, handler);

						lock(client._registeredEventsLock)
						{
							var registeredEvents = client._registeredEvents;
							if (registeredEvents != null)
							{
								Dictionary<Delegate, WeakCollection<object>> delegateDictionary;
								if (registeredEvents.TryGetValue(eventInfo, out delegateDictionary))
								{
									WeakCollection<object> weakList;
									if (delegateDictionary.TryGetValue(handler, out weakList))
										weakList.Remove(obj);
								}
							}
						}
					}
					catch(RemotingSyncException)
					{
						throw;
					}
					catch(Exception exception)
					{
						if (after == null || !args.CanInvoke)
							throw;

						args.Exception = exception;
					}
					finally
					{
						if (args != null)
						{
							if (after != null && args.CanInvoke)
								after(this, args);

							var ex = args.Exception;
							if (ex != null)
								throw ex;
						}
					}

					return null;
				}
			);
		}
	}
}
