﻿
namespace Pfz.Remoting
{
	/// <summary>
	/// Interface that must be implemented by Serializable objects to mark them as acessible to the remoting framework.
	/// </summary>
	public interface IRemotable
	{
	}
}
