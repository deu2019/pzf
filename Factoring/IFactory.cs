﻿using System;

namespace Pfz.Factoring
{
	/// <summary>
	/// Interface used to access a factory without knowing its details.
	/// </summary>
	public interface IFactory
	{
		/// <summary>
		/// Gets the base type of the factory.
		/// </summary>
		Type FactoryBaseType { get; }

		/// <summary>
		/// Gets the BaseDataType to which this factory is prepared to work for.
		/// For example, a factory registered to any object may have an actual datatype of String, but its
		/// base data type will still be object.
		/// </summary>
		Type BaseDataType { get; }

		/// <summary>
		/// Gets the DataType that instances created by this factory are prepared to deal with.
		/// </summary>
		Type DataType { get; }

		/// <summary>
		/// Creates the appropriate editor/searcher for the data-type.
		/// </summary>
		object Create();
	}

	/// <summary>
	/// Typed version of IFactory.
	/// </summary>
	public interface IFactory<out T>:
		IFactory
	{
		/// <summary>
		/// Creates the appropriate editor/searcher for the data-type.
		/// </summary>
		new T Create();
	}
}
