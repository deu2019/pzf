﻿using System.Collections.Generic;

namespace Pfz.Factoring
{
	/// <summary>
	/// Interface that must be implemented by "Searchers" or "Lookups".
	/// </summary>
	[FactoryBase]
	public interface ISearcher
	{
		/// <summary>
		/// Fills the default parameters (if any) to be used by this searcher.
		/// </summary>
		/// <param name="value"></param>
		void FillDefaultParameters(object value);

		/// <summary>
		/// Gets or sets a value indicating if this searcher allows multi-select.
		/// </summary>
		bool MustAllowMultiSelect { get; set; }

		/// <summary>
		/// Gets a collection with all the results found by this searcher.
		/// </summary>
		ICollection<object> GetResults();
	}
}
