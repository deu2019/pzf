﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pfz.Factoring
{
	/// <summary>
	/// Delegate used by the parallel-type mapper class to register
	/// a non-default action when mapping a source-type to a destination one.
	/// </summary>
	public delegate object ParallelTypeMapperCreateDelegate(ParallelTypeMapper typeMapper, object sourceInstance);
}
