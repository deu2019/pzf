﻿
namespace Pfz.Factoring
{
	/// <summary>
	/// Interface that must be implemented by data editors.
	/// </summary>
	[FactoryBase]
	public interface IEditor
	{
		/// <summary>
		/// Gets or sets the actual record for this editor. Note that the actual record may not reflect all fields
		/// filled by the user. This record must be editable (not-read-only).
		/// </summary>
		object Record { get; set; }

		/// <summary>
		/// Requests the editor to apply all the actual updates to the record and then return it.
		/// </summary>
		object ApplyChanges();
	}
}
