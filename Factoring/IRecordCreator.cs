﻿
namespace Pfz.Factoring
{
	/// <summary>
	/// Interface used (generally by ISearchers) to tell that they are also capable of creating new records.
	/// </summary>
	public interface IRecordCreator
	{
		/// <summary>
		/// Creates a new record.
		/// </summary>
		object CreateRecord();
	}
}
