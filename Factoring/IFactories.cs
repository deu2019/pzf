﻿using System;

namespace Pfz.Factoring
{
	/// <summary>
	/// Interface used by factory generators, or factory of factories.
	/// </summary>
	public interface IFactories
	{
		/// <summary>
		/// Tries to create a factory for the given dataType.
		/// </summary>
		IFactory TryCreateFactory(Type dataType);

		/// <summary>
		/// Create a factory for the given dataType or throws an exception.
		/// </summary>
		IFactory CreateFactory(Type dataType);


		/// <summary>
		/// Tries to create a factory for the given dataType.
		/// </summary>
		IFactory TryCreateFactory<DataType>();

		/// <summary>
		/// Create a factory for the given dataType or throws an exception.
		/// </summary>
		IFactory CreateFactory<DataType>();
	}

	/// <summary>
	/// Typed version of IFactories.
	/// </summary>
	public interface IFactories<out T>:
		IFactories
	{
		/// <summary>
		/// Tries to create a factory for the given dataType.
		/// </summary>
		new IFactory<T> TryCreateFactory(Type dataType);

		/// <summary>
		/// Create a factory for the given dataType or throws an exception.
		/// </summary>
		new IFactory<T> CreateFactory(Type dataType);

		/// <summary>
		/// Tries to create a factory for the given dataType.
		/// </summary>
		new IFactory<T> TryCreateFactory<DataType>();

		/// <summary>
		/// Create a factory for the given dataType or throws an exception.
		/// </summary>
		new IFactory<T> CreateFactory<DataType>();
	}
}
