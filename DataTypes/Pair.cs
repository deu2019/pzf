﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pfz.DataTypes
{
	/// <summary>
	/// Helper class to create KeyValuePair instances without typing a lot.
	/// </summary>
	public static class Pair
	{
		/// <summary>
		/// Creates a new key-value pair, but allows type-infering.
		/// </summary>
		public static KeyValuePair<TKey, TValue> Create<TKey, TValue>(TKey key, TValue value)
		{
			return new KeyValuePair<TKey, TValue>(key, value);
		}
	}
}
