﻿
namespace Pfz.DataTypes
{
	/// <summary>
	/// Untyped value container.
	/// </summary>
	public interface IValueContainer
	{
		/// <summary>
		/// Gets or sets the value of this container.
		/// </summary>
		object Value { get; set; }
	}

	/// <summary>
	/// Typed value container.
	/// </summary>
	public interface IValueContainer<T>:
		IValueContainer
	{
		/// <summary>
		/// Gets or sets the rightly typed value of this container.
		/// </summary>
		new T Value { get; set; }
	}
}
