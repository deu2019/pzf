﻿
namespace System.Data
{
	/// <summary>
	/// Implementing this exception here only because Windows Phone doesn't have it.
	/// I plan to delete this code in the future.
	/// </summary>
	public class ReadOnlyException:
		Exception
	{
		/// <summary>
		/// Creates a new exception without a message.
		/// </summary>
		public ReadOnlyException()
		{
		}

		/// <summary>
		/// Creates a new exception with a message.
		/// </summary>
		public ReadOnlyException(string message):
			base(message)
		{
		}
	}
}
