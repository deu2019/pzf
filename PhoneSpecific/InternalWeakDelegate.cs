﻿using System;
using System.Reflection;

namespace Pfz.Caching
{
	/// <summary>
	/// A class used as the base class to implement weak delegates.
	/// See WeakDelegate.From method implementations to see how it works.
	/// </summary>
	internal sealed class InternalWeakDelegate
	{
		private WeakReference _weakReference;

		#region Constructor
			/// <summary>
			/// Creates this weak-delegate class based as a copy of the given 
			/// delegate handler.
			/// </summary>
			/// <param name="handler">The handler to copy information from.</param>
			public InternalWeakDelegate(Delegate handler)
			{
				if (!handler.Method.IsPublic)
					throw new ArgumentException("handler must be public in windows-phone.\r\nSource: " + handler.Method.DeclaringType.FullName + "." + handler.Method.Name, "handler");

				_weakReference = new WeakReference(handler.Target);
				Method = handler.Method;
			}
		#endregion
		
		#region Method
			/// <summary>
			/// Gets the method used by this delegate.
			/// </summary>
			public MethodInfo Method { get; private set; }

			/// <summary>
			/// Gets or sets the target of this weak-reference.
			/// </summary>
			public object Target
			{
				get
				{
					return _weakReference.Target;
				}
				set
				{
					_weakReference.Target = value;
				}
			}

		#endregion
		#region Invoke
			/// <summary>
			/// Invokes this delegate with the given parameters.
			/// </summary>
			/// <param name="parameters">The parameters to be used by the delegate.</param>
			public void Invoke(object[] parameters)
			{
				object target = _weakReference.Target;
				if (target != null || Method.IsStatic)
					Method.Invoke(target, parameters);
			}
		#endregion
	}
}
