﻿
namespace System
{
	/// <summary>
	/// It is here only to allow already existing classes (like exception) with the serializable
	/// attribute to compile.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public sealed class Serializable:
		Attribute
	{

	}
}
