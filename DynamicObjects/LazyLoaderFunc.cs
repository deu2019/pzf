﻿
namespace Pfz.DynamicObjects
{
	/// <summary>
	/// Delegate used to create a "lazy loader" for a property with
	/// the given name. In general, such lazy loader will use the property
	/// name as the file name, table name or something similar.
	/// </summary>
	public delegate T LazyLoaderFunc<T>(string propertyName);

	/// <summary>
	/// Delegate used to create a "lazy loader" for a property with
	/// the given name. In general, such lazy loader will use the property
	/// name as the file name, table name or something similar.
	/// </summary>
	public delegate T LazyLoaderFunc<TUserInstance, T>(TUserInstance instance, string propertyName);
}
