﻿
namespace Pfz.DynamicObjects
{
	/// <summary>
	/// Action invoked by the type-builder when a dynamic method is added.
	/// Actions like this can be used for all methods, independent on the number of arguments,
	/// as those are all received as an array of values.
	/// </summary>
	public delegate object DelegatedDynamicAction<T>(T userInstance, object[] values);
}
