﻿
using Pfz.DynamicObjects.CloneToModifyModel;
namespace Pfz.Extensions
{
	/// <summary>
	/// Adds typed methods to ICtmObject objects.
	/// </summary>
	public static class CtmExtensions
	{
		/// <summary>
		/// If this is a modifiable object, gets the read-only source that generated it.
		/// Such value can be null if this is the first object (you just created a modifiable
		/// one).
		/// If this is a read-only object, even if it was copied from another one, the
		/// returns is always null.
		/// </summary>
		public static T GetOldInstance<T>(this T source)
		where
			T: ICtmObject
		{
			return (T)source.UntypedGetOldInstance();
		}

		/// <summary>
		/// Gets a read-only view of the actual object.
		/// If this object is already read-only, "this" is returned.
		/// If not, a read-only copy is made. Note that this may differ
		/// from the GetReadOnlySource() result if this is an already changed
		/// copy.
		/// </summary>
		public static T AsReadOnly<T>(this T source, bool keepOldInstance)
		where
			T: ICtmObject
		{
			return (T)source.UntypedAsReadOnly(keepOldInstance);
		}

		/// <summary>
		/// If this object is already modifiable, this is returned.
		/// Otherwise a modifiable clone is returned. It may throw a 
		/// ReadOnlyException if the interface is marked as NonUpdatable.
		/// </summary>
		public static T AsModifiable<T>(this T source)
		where
			T: ICtmObject
		{
			return (T)source.UntypedAsModifiable();
		}
	}
}
