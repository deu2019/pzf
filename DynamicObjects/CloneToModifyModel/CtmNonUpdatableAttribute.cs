﻿using System;

namespace Pfz.DynamicObjects.CloneToModifyModel
{
	/// <summary>
	/// Use this property over a type to tell than it is not "updatable". That is,
	/// its values can be changed when it is first created, but after it is clonned, it can
	/// never be changed again.
	/// If used over properties, tells that such properties can't be changed on clones, but
	/// the object itself is still changeable.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Property, AllowMultiple=false, Inherited=true)]
	public sealed class CtmNonUpdatableAttribute:
		Attribute
	{
	}
}
