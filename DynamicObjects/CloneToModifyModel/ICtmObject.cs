﻿using System.ComponentModel;
using System;

namespace Pfz.DynamicObjects.CloneToModifyModel
{
	/// <summary>
	/// Interface used to describa a CloneToModify object.
	/// This interface is automatically implemented by the CtmGenerator class.
	/// </summary>
	public interface ICtmObject:
		INotifyPropertyChanged
	{
		/// <summary>
		/// Gets the CtmType that describes information exclusive to
		/// Clone-to-Modify types.
		/// </summary>
		CtmType GetCtmType();
	
		/// <summary>
		/// Gets a value indicating if this instance is read-only.
		/// </summary>
		bool GetIsReadOnly();

		/// <summary>
		/// For representions of updates, gets a read-only representation of the original
		/// values used to update this object. With that you can, for example, generate
		/// an update sql only of those properties that effectively changed.
		/// Such value can be null if this is an "insert" record. That is, the record
		/// that was just created.
		/// </summary>
		object UntypedGetOldInstance();

		/// <summary>
		/// Gets a read-only view of the actual object.
		/// If the object is already read-only and the keepOldInstance is
		/// compatible, this is returned.
		/// </summary>
		object UntypedAsReadOnly(bool keepOldInstance);

		/// <summary>
		/// Gets a modifiable version of this object.
		/// If it is already modifiable, this is returned. If it is not, then
		/// a modifiable copy is made.
		/// </summary>
		object UntypedAsModifiable();
	}
}
