﻿using System;
using System.Collections.ObjectModel;

namespace Pfz.DynamicObjects.CloneToModifyModel
{
	/// <summary>
	/// For auto implemented CtmObjects this attribute should be used over
	/// calculated properties to tell on which properties it is based. This will make
	/// calls to PropertyChanged events to the property with this attribute when
	/// the any of the source properties is changed.
	/// </summary>
	[CLSCompliant(false)]
	[AttributeUsage(AttributeTargets.Property, AllowMultiple=false)]
	public sealed class CtmCalculatedPropertyAttribute:
		Attribute
	{
		/// <summary>
		/// Creates a new instance of this attribute telling on which properties
		/// the actual calculated property is based.
		/// </summary>
		public CtmCalculatedPropertyAttribute(params string[] propertyNames)
		{
			if (propertyNames == null)
				throw new ArgumentNullException("propertyNames");

			if (propertyNames.Length == 0)
				throw new ArgumentException("propertyNames can't be empty.", "propertyNames");

			foreach(var propertyName in propertyNames)
				if (propertyName == null)
					throw new ArgumentException("propertyNames can't contain null values.", "propertyNames");

			_propertyNames = new ReadOnlyCollection<string>(propertyNames);
		}

		private readonly ReadOnlyCollection<string> _propertyNames;
		/// <summary>
		/// Gets the names of the properties used to calculate the value of the
		/// property that contains this attribute.
		/// </summary>
		public ReadOnlyCollection<string> PropertyNames
		{
			get
			{
				return _propertyNames;
			}
		}
	}
}
