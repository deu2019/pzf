﻿using System;

namespace Pfz.DynamicObjects.CloneToModifyModel
{
	/// <summary>
	/// Represents CloneToModify specific information of a type.
	/// </summary>
	public sealed class CtmType
	{
		/// <summary>
		/// Creates a new CtmType.
		/// This should not be called by users. It is here because
		/// the auto-generated code needs it public.
		/// </summary>
		public CtmType(Type abstractType, bool isNonUpdatable)
		{
			AbstractType = abstractType;
			IsNonUpdatable = isNonUpdatable;
		}
		
		/// <summary>
		/// Gets the abstract-type that was used as parameter to the
		/// CtmGenerator to create an instance of this type.
		/// </summary>
		public Type AbstractType { get; private set; }
		
		/// <summary>
		/// Gets a value indicating if instances of this type
		/// are non-updatable (that is, no property can
		/// be updated after creation).
		/// </summary>
		public bool IsNonUpdatable { get; private set; }
	}
}
