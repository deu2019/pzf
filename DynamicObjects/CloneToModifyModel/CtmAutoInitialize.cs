﻿using Pfz.Extensions;

namespace Pfz.DynamicObjects.CloneToModifyModel
{
	/// <summary>
	/// Properties whose type is derived from CtmAutoInitialize must be GET only.
	/// Those properties are auto-initialized (with the default constructor) and
	/// they will never be modifiable. But, their content can still be modified
	/// but to do that the implementor should check if the instance itself is
	/// modifiable or not.
	/// </summary>
	/// TODO pass the IsReadOnly as a constructor parameter. For Non-Updatable
	/// properties, it should be IsReadOnly independent of the state of the object.
	public abstract class CtmAutoInitialize:
		ICtmInternalCopyValues
	{
		/// <summary>
		/// Instantiates a new CtmAutoInitialize property telling 
		/// the owner and if it is updatable or not.
		/// </summary>
		protected CtmAutoInitialize(CtmObject owner, bool isNonUpdatable)
		{
			Owner = owner;

			IsNonUpdatable = isNonUpdatable;
		}
		
		/// <summary>
		/// Gets the owner of this CtmAutoInitialize instance.
		/// </summary>
		protected CtmObject Owner { get; private set; }
		
		/// <summary>
		/// Gets a valud indicating if this AutoInitialize property
		/// is non-updatable (that is, after creation it is no
		/// more modifiable).
		/// </summary>
		public bool IsNonUpdatable { get; private set; }
		
		/// <summary>
		/// Gets a value indicating if this property is ReadOnly.
		/// This is true if the Owner is read-only or if it is non-updatable
		/// and the owner is a copy or the create record.
		/// </summary>
		public bool IsReadOnly
		{
			get
			{
				return Owner.GetIsReadOnly() || (IsNonUpdatable && Owner.GetOldInstance() != null);
			}
		}
		
		/// <summary>
		/// Copy the contents of this AutoInitialize object to another one.
		/// You should not call this manually, but you should implement it
		/// so clones are created correctly.
		/// </summary>
		protected abstract void CopyValuesTo(CtmAutoInitialize clone);

		#region ICtmInternalCopyValues Membres
			void ICtmInternalCopyValues.CopyValuesTo(CtmAutoInitialize clone)
			{
				CopyValuesTo(clone);
			}
		#endregion
	}
}
