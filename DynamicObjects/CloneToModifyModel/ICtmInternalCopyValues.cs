﻿
namespace Pfz.DynamicObjects.CloneToModifyModel
{
	/// <summary>
	/// This interface is needed by the generated code (and so must be public).
	/// You should never use it directly.
	/// </summary>
	public interface ICtmInternalCopyValues
	{
		/// <summary>
		/// Copy values from this instance to a clone, even if it
		/// is marked as read-only.
		/// </summary>
		void CopyValuesTo(CtmAutoInitialize clone);
	}
}
