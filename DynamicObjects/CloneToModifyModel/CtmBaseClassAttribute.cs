﻿using System;

namespace Pfz.DynamicObjects.CloneToModifyModel
{
	/// <summary>
	/// This attribute tells that while auto-implementing an interface with
	/// this attribute a special base class is required (which must be a child
	/// of CtmObject).
	/// </summary>
	[AttributeUsage(AttributeTargets.Interface, AllowMultiple=false)]
	public sealed class CtmBaseClassAttribute:
		Attribute
	{
		/// <summary>
		/// Creates a new CtmBaseClassAttribute that should use the given
		/// Type as the base one.
		/// </summary>
		public CtmBaseClassAttribute(Type type)
		{
			if (type == null)
				throw new ArgumentNullException("type");
				
			if (type.IsInterface)
				throw new ArgumentException("type must not be an interface.", "type");
				
			Type = type;
		}
		
		/// <summary>
		/// Gets the Type of the BaseClass that should be used for the
		/// interface that has this attribute.
		/// </summary>
		public Type Type { get; private set; }
	}
}
