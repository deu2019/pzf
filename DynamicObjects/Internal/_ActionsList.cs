﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pfz.DynamicObjects.Internal
{
	internal sealed class _ActionsList:
		List<Action>,
		IDisposable
	{
		public void Dispose()
		{
			foreach(var item in this)
				item();
		
			Clear();
		}
	}
}
