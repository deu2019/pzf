﻿
namespace Pfz.DynamicObjects
{
	/// <summary>
	/// Delegated used to initialize a generated property-field.
	/// </summary>
	public delegate TField DelegatedFieldInitialize<TUserInstance, TField>(TUserInstance userInstance);
}
