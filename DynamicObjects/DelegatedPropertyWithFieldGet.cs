﻿
namespace Pfz.DynamicObjects
{
	/// <summary>
	/// Delegate used by DelegatedTypeBuilder.AddPropertyWithField for the get action.
	/// </summary>
	public delegate TProperty DelegatedPropertyWithFieldGet<TUserInstance, TProperty, TField>(TUserInstance instance, TField fieldValue);
}
