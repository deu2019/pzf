﻿
namespace Pfz.DynamicObjects
{
	/// <summary>
	/// Delegate used by DelegatedTypeBuilder.AddPropertyWithField for the set action.
	/// </summary>
	public delegate void DelegatedPropertyWithFieldSet<TUserInstance, TProperty, TField>(TUserInstance instance, ref TField fieldValue, TProperty newValue);
}
