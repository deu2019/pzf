﻿
namespace Pfz.DynamicObjects
{
	/// <summary>
	/// Delegate used by thge DelegatedTypeBuilder when generating a new event.
	/// Both add and remove use this delegate type.
	/// </summary>
	public delegate void DelegatedEventAddRemove<TUserInstance, TDelegate>(TUserInstance instance, TDelegate handler);
}
