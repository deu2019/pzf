﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Pfz.ErrorHandling
{
	/// <summary>
	/// Error thrown by the ErrorPack if the errors are not solved.
	/// </summary>
	[Serializable]
	public sealed class ErrorPackException:
		Exception
	{
		/// <summary>
		/// Generates an error pack with a single error.
		/// </summary>
		public ErrorPackException(object error):
			base("An error was generated and was not dealt with.")
		{
			_errors = new ReadOnlyCollection<object>(new object[]{error});
		}

		/// <summary>
		/// Generates an error pack with the following list of errors.
		/// </summary>
		public ErrorPackException(ReadOnlyCollection<object> errors):
			base(string.Concat("A total of ", errors.Count, " error(s) were generated."))
		{
			_errors = errors;
		}
		
		private readonly ReadOnlyCollection<object> _errors;
		/// <summary>
		/// Gets a collection with all unsolved errors.
		/// </summary>
		public ReadOnlyCollection<object> Errors
		{
			get
			{
				return _errors;
			}
		}

		internal ErrorPackException(SerializationInfo info, StreamingContext context)
		{
			_errors = (ReadOnlyCollection<object>)info.GetValue("_errors", typeof(ReadOnlyCollection<object>));
		}
		
		/// <summary>
		/// Adds the Errors to the serialization info.
		/// </summary>
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);

			info.AddValue("_errors", _errors);
		}
	}
}
