﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Pfz.ErrorHandling
{
	internal sealed class _ErrorPack
	{
		internal readonly ErrorRepackBase _baseErrorPack;
		internal ErrorRepackBase _actualErrorPack;
		
		internal _ErrorPack(ErrorRepackBase baseErrorPack)
		{
			_baseErrorPack = baseErrorPack;
		}
	
		public bool Dispose(ErrorRepackBase caller)
		{
			if (caller != _baseErrorPack)
				return false;
		
			ErrorPack._current = null;
			
			if (_baseErrorPack._errors.Count > 0)
				throw new ErrorPackException(_baseErrorPack._roErrors);
				
			return true;
		}
	}
}
