﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;

namespace Pfz.ErrorHandling
{
	/// <summary>
	/// Class that packs the new errors as errors of a given property.
	/// </summary>
	public sealed class PropertyErrorPack:
		ErrorRepackBase
	{
		/// <summary>
		/// Creates a property error pack for the given property.
		/// You muse dispose this instance for the packing to work.
		/// </summary>
		/// <param name="property"></param>
		public PropertyErrorPack(PropertyInfo property):
			base(property)
		{
			if (property == null)
				throw new ArgumentNullException("property");
				
			Property = property;
		}
		
		/// <summary>
		/// Gets the property that will be considered the responsible for the
		/// possible errors.
		/// </summary>
		public PropertyInfo Property { get; private set; }

		/// <summary>
		/// Packs the errors as a PropertyErrors object.
		/// </summary>
		protected override object CreatePack(ReadOnlyCollection<object> errors)
		{
			return new PropertyErrors(Property, errors);
		}
	}
}
