﻿using System;
using System.Collections.ObjectModel;

namespace Pfz.ErrorHandling
{
	/// <summary>
	/// Class that stores many errors related to a "record".
	/// </summary>
	public sealed class RecordErrors
	{
		/// <summary>
		/// Creates a new RecordErrors instance.
		/// </summary>
		public RecordErrors(object record, ReadOnlyCollection<object> errors)
		{
			if (record == null)
				throw new ArgumentNullException("record");
				
			if (errors == null)
				throw new ArgumentNullException("errors");
				
			if (errors.Count == 0)
				throw new ArgumentException("errors is empty.", "errors");
				
			Record = record;
			Errors = errors;
		}
		
		/// <summary>
		/// Gets the Record.
		/// </summary>
		public object Record { get; private set ; }
		
		/// <summary>
		/// Gets the Errors.
		/// </summary>
		public ReadOnlyCollection<object> Errors { get; private set; }
	}
}
