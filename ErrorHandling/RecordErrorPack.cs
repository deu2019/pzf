﻿using System;
using System.Collections.ObjectModel;

namespace Pfz.ErrorHandling
{
	/// <summary>
	/// Class that packs the next errors as errors related to a record.
	/// </summary>
	public sealed class RecordErrorPack:
		ErrorRepackBase
	{
		/// <summary>
		/// Creates a new record error pack for the given record.
		/// You must dispose this instance to pack the errors.
		/// </summary>
		public RecordErrorPack(object record):
			base(record)
		{
			if (record == null)
				throw new ArgumentNullException("record");
				
			Record = record;
		}

		/// <summary>
		/// Gets the Record to bound errors to.
		/// </summary>
		public object Record { get; private set; }

		/// <summary>
		/// Returns a RecordErrors with all the errors.
		/// </summary>
		protected override object CreatePack(ReadOnlyCollection<object> errors)
		{
			return new RecordErrors(Record, errors);
		}
	}
}
