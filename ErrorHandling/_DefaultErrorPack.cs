﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Pfz.ErrorHandling
{
	internal sealed class _DefaultErrorPack:
		ErrorRepackBase
	{
		private static readonly object _key = new object();

		internal _DefaultErrorPack():
			base(_key)
		{
		}

		protected override object CreatePack(ReadOnlyCollection<object> errors)
		{
			return errors;
		}
	}
}
