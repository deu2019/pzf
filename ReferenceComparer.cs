﻿using System.Collections.Generic;

namespace Pfz
{
	#if SILVERLIGHT
		/// <summary>
		/// Class used to compare two references.
		/// They must point to the same instance (not an equal instance) to be
		/// considered equal.
		/// </summary>
		public sealed class ReferenceComparer<T>:
			IEqualityComparer<T>
		where
			T: class
		{
			/// <summary>
			/// Gets the ReferenceComparer single instance.
			/// </summary>
			public static readonly ReferenceComparer<T> Instance = new ReferenceComparer<T>();

			private ReferenceComparer()
			{
			}

			bool IEqualityComparer<T>.Equals(T x, T y)
			{
				return x == y;
			}
			int IEqualityComparer<T>.GetHashCode(T obj)
			{
				if (obj == null)
					return -1;

				return obj.GetHashCode();
			}
		}
	#else
		/// <summary>
		/// Class used to compare two references.
		/// They must point to the same instance (not an equal instance) to be
		/// considered equal.
		/// </summary>
		public sealed class ReferenceComparer:
			IEqualityComparer<object>
		{
			/// <summary>
			/// Gets the ReferenceComparer single instance.
			/// </summary>
			public static readonly ReferenceComparer Instance = new ReferenceComparer();

			private ReferenceComparer()
			{
			}

			bool IEqualityComparer<object>.Equals(object x, object y)
			{
				return x == y;
			}
			int IEqualityComparer<object>.GetHashCode(object obj)
			{
				if (obj == null)
					return -1;

				return obj.GetHashCode();
			}
		}
	#endif
}
