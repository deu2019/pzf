﻿using System.Threading;
using Pfz.Threading;

namespace Pfz.Extensions
{
    /// <summary>
    /// Adds the AbortIfSafe and an Abort overload, which will use AbortIfSafe.
    /// </summary>
    public static class PfzThreadExtensions
    {
        /// <summary>
        /// Aborts a thread only if it is safe to do so (the thread is not constructing an IDisposable object).
        /// It may still be useful to force a full-collection to deallocate any full-constructed but not assigned disposable object.
        /// Returns if the Thread.Abort() was called or not.
        /// </summary>
        public static bool AbortIfSafe(this Thread thread, SafeAbortMode mode = SafeAbortMode.RunAllValidations, object stateInfo = null)
        {
            return SafeAbort.AbortIfSafe(thread, mode, stateInfo);
        }

        /// <summary>
        /// Aborts a thread, trying to use the safest abort mode, until the unsafest one.
        /// The number of retries is also the expected number of milliseconds trying to abort.
        /// </summary>
        public static bool Abort(this Thread thread, int triesWithAllValidations, int triesIgnoringUserValidations, int triesAllowingUsingToFail, bool finalizeWithNormalAbort = false, object stateInfo = null)
        {
            return SafeAbort.Abort(thread, triesWithAllValidations, triesIgnoringUserValidations, triesAllowingUsingToFail, finalizeWithNormalAbort, stateInfo);
        }
    }
}
