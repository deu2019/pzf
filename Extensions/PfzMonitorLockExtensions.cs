﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using Pfz.Threading;
using Pfz.Threading.Disposers;

namespace Pfz.Extensions.MonitorLockExtensions
{
	/// <summary>
	/// Adds methods to lock any object using Monitor methods easily and
	/// always with time-out, so you can avoid dead-locks.
	/// See PfzLockConfiguration class if you want to log dead-locks.
	/// </summary>
	public static class PfzMonitorLockExtensions
	{
		#region LockWithTimeout - Disposable
			/// <summary>
			/// Tries to acquire a lock on the given object, using the default lock-timeout.
			/// In case of failure, it logs the error, but does not generates an exception. Instead, it returns
			/// null.
			/// </summary>
			/// <typeparam name="T">The type of class to lock.</typeparam>
			/// <param name="item">The item to lock.</param>
			/// <returns>A disposable object, so you can release the lock, or null if the lock was not acquired.</returns>
			public static MonitorLockDisposer TryLockWithTimeout<T>(this T item)
			where
				T: class
			{
				return TryLockWithTimeout(item, LockConfiguration.DefaultLockTimeout);
			}
			
			/// <summary>
			/// Tries to acquire a lock on the given object, using the given time-out.
			/// In case of failure, it logs the error, but does not generates an exception. Instead, it returns
			/// null.
			/// </summary>
			/// <typeparam name="T">The type of class to lock.</typeparam>
			/// <param name="item">The item to lock.</param>
			/// <param name="timeoutInMilliseconds">The timeout value while trying to acquire the lock.</param>
			/// <returns>A disposable object, so you can release the lock, or null if the lock was not acquired.</returns>
			[SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
			public static MonitorLockDisposer TryLockWithTimeout<T>(this T item, int timeoutInMilliseconds)
			{
				MonitorLockDisposer result = new MonitorLockDisposer(item, timeoutInMilliseconds);
				if (result._lockTaken)
					return result;

				return null;
			}
		#endregion
		
		#region Lock
			/// <summary>
			/// Locks the actual object and returns a Disposable object.
			/// You can, then, dispose it before the end of the block is
			/// achieved with using, but without calling excessive exits.
			/// This is not abort-safe.
			/// </summary>
			[SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
			public static MonitorLockDisposer Lock(this object item)
			{
				MonitorLockDisposer result = new MonitorLockDisposer(item, Timeout.Infinite);
				return result;
			}
		#endregion
	}
}
