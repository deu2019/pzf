﻿using System.Runtime.InteropServices;
using Pfz.DataTypes;

namespace Pfz.Caching
{
	/// <summary>
	/// Interface used by 
	/// </summary>
	public interface IReferenceData:
		IValueContainer
	{
		/// <summary>
		/// Gets the HandleType or this reference-data.
		/// </summary>
		GCHandleType HandleType { get; }
	}

	/// <summary>
	/// Typed version of IReferenceData.
	/// </summary>
	public interface IReferenceData<T>:
		IReferenceData,
		IValueContainer<T>
	{
	}
}
