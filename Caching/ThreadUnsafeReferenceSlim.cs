﻿using System;
using System.Runtime.InteropServices;
using Pfz.DataTypes;

namespace Pfz.Caching
{
	/// <summary>
	/// This is the struct that implements the code for ThreadUnsafeReference and
	/// Reference.
	/// </summary>
	public struct ThreadUnsafeReferenceSlim<T>:
		IReference<T>,
		IEquatable<IReference<T>>,
		ICloneable<ThreadUnsafeReferenceSlim<T>>,
		IDisposable
	where
		T: class
	{
		private GCHandle _handle;

		/// <summary>
		/// Creates a new reference using the given value and handle type.
		/// </summary>
		public ThreadUnsafeReferenceSlim(T value, GCHandleType handleType)
		{
			try
			{
			}
			finally
			{
				_handle = GCHandle.Alloc(value, handleType);
			}
			_handleType = handleType;
		}
		
		/// <summary>
		/// Frees the handle immediatelly.
		/// </summary>
		public void Dispose()
		{
			var handle = _handle;
			if (handle.IsAllocated)
			{
				try
				{
				}
				finally
				{
					_handle = new GCHandle();
					handle.Free();
				}
			}
		}
		
		/// <summary>
		/// Gets or sets the value pointed by the handle.
		/// </summary>
		public T Value
		{
			get
			{
				return (T)_handle.Target;
			}
			set
			{
				_handle.Target = value;
			}
		}
		
		private GCHandleType _handleType;
		
		/// <summary>
		/// Gets or sets the handle type.
		/// </summary>
		public GCHandleType HandleType
		{
			get
			{
				return _handleType;
			}
			set
			{
				if (value == _handleType)
					return;
				
				GCHandle oldHandle = _handle;
				try
				{
				}
				finally
				{
					// this block must be executed completelly or must not be executed.
					_handle = GCHandle.Alloc(_handle.Target, value);
					_handleType = value;
					oldHandle.Free();
				}
			}
		}

		/// <summary>
		/// Gets the data of this Reference.
		/// </summary>
		public ReferenceData<T> GetData()
		{
			return new ReferenceData<T>((T)_handle.Target, _handleType);
		}

		/// <summary>
		/// Sets the data of this reference.
		/// </summary>
		public void Set(GCHandleType handleType, T value)
		{
			if (handleType == _handleType)
				_handle.Target = value;
			else
			{
				GCHandle oldHandle = _handle;
				try
				{
				}
				finally
				{
					// this block must be executed completelly or must not be executed.
					_handle = GCHandle.Alloc(value, handleType);
					_handleType = handleType;
					oldHandle.Free();
				}
			}
		}

		/// <summary>
		/// Gets the HashCode of the Value.
		/// </summary>
		public override int GetHashCode()
		{
			var value = Value;
			if (value == null)
				return 0;

			return value.GetHashCode();
		}

		/// <summary>
		/// Compares the Value of this reference with the value of another reference.
		/// </summary>
		public override bool Equals(object obj)
		{
			var other = obj as IReference<T>;
			if (other != null)
				return Equals(other);

			return false;
		}

		/// <summary>
		/// Compares the Value of this reference with the value of another reference.
		/// </summary>
		public bool Equals(IReference<T> other)
		{
			if (other == null)
				return false;

			return Value == other.Value;
		}
		
		#region IValueContainer Members
			object IValueContainer.Value
			{
				get
				{
					return _handle.Target;
				}
				set
				{
					Value = (T)value;
				}
			}
		#endregion
		#region ICloneable<ThreadUnsafeReference<T>> Members
			/// <summary>
			/// Creates a copy of this reference.
			/// </summary>
			public ThreadUnsafeReferenceSlim<T> Clone()
			{
				return new ThreadUnsafeReferenceSlim<T>((T)_handle.Target, _handleType);
			}
		#endregion
		#region ICloneable Members
			#if !WINDOWS_PHONE
			object ICloneable.Clone()
			{
				return Clone();
			}
			#endif
		#endregion
		#region IReference Members
			void IReference.Set(GCHandleType handleType, object value)
			{
				Set(handleType, (T)value);
			}
			IReferenceData IReference.GetData()
			{
				return GetData();
			}
		#endregion
	}
}
