﻿
namespace Pfz.Serialization
{
	/// <summary>
	/// Struct that holds information about an item serializer and the id it received.
	/// </summary>
	public struct ItemSerializerReference
	{
		/// <summary>
		/// Creates a new ItemSerializerReference filling its values.
		/// </summary>
		public ItemSerializerReference(int id, IItemSerializer itemSerializer)
		{
			_id = id;
			_itemSerializer = itemSerializer;
		}

		private int _id;
		/// <summary>
		/// Gets the Id given to the ItemSerializer.
		/// </summary>
		public int Id
		{
			get
			{
				return _id;
			}
		}

		private IItemSerializer _itemSerializer;
		/// <summary>
		/// Gets the ItemSerializer.
		/// </summary>
		public IItemSerializer ItemSerializer
		{
			get
			{
				return _itemSerializer;
			}
		}
	}
}
