﻿using System;
using Pfz.Extensions;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializer for Double. Must be instantiated.
	/// </summary>
	public sealed class DoubleSerializer:
		ItemSerializer<double>
	{
		/// <summary>
		/// Serializes a double value.
		/// </summary>
		public override void Serialize(ConfigurableSerializerBase serializer, double value)
		{
			byte[] bytes = BitConverter.GetBytes(value);
			serializer.Stream.Write(bytes, 0, 8);
		}

		private byte[] _bytes = new byte[8];
		/// <summary>
		/// Deserializes a double value. This method is not thread-safe.
		/// </summary>
		public override double Deserialize(ConfigurableSerializerBase deserializer)
		{
			deserializer.Stream.FullRead(_bytes);
			double result = BitConverter.ToDouble(_bytes, 0);
			return result;
		}
	}
}
