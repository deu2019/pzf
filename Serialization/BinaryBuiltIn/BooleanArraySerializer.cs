﻿using Pfz.Extensions;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializes a boolean array (compressed).
	/// </summary>
	public sealed class BooleanArraySerializer:
		ItemSerializer<bool[]>
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly BooleanArraySerializer Instance = new BooleanArraySerializer();

		private BooleanArraySerializer()
		{
		}

		/// <summary>
		/// Serializes a boolean array.
		/// </summary>
		public override void Serialize(ConfigurableSerializerBase serializer, bool[] item)
		{
			var stream = serializer.Stream;
			stream.WriteCompressedInt32(item.Length);
			stream.WriteBooleanArray(item);
		}

		/// <summary>
		/// Deserializes a boolean array.
		/// </summary>
		public override bool[] Deserialize(ConfigurableSerializerBase deserializer)
		{
			var stream = deserializer.Stream;
			int count = stream.ReadCompressedInt32();
			bool[] result = new bool[count];
			stream.ReadBooleanArray(result);
			return result;
		}
	}
}
