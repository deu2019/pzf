﻿using System;
using Pfz.Extensions;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializer for System.Type
	/// </summary>
	public sealed class TypeSerializer:
		IItemSerializer
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly TypeSerializer Instance = new TypeSerializer();

		private TypeSerializer()
		{
		}

		/// <summary>
		/// Serializes a type.
		/// </summary>
		public void Serialize(ConfigurableSerializerBase serializer, Type type)
		{
			var stream = serializer.Stream;

			var reference = serializer.TryGetItemSerializer(type);
			if (reference.ItemSerializer != null)
			{
				stream.WriteCompressedInt32(reference.Id+1);
				return;
			}

			stream.WriteByte(0);
			AssemblySerializer.Instance.Serialize(serializer, type.Assembly);
			stream.WriteString(type.FullName);
		}

		/// <summary>
		/// Deserializes a type.
		/// </summary>
		public Type Deserialize(ConfigurableSerializerBase deserializer)
		{
			var stream = deserializer.Stream;
			int referenceId = stream.ReadCompressedInt32();

			if (referenceId > 0)
				return deserializer.GetTypeById(referenceId-1);

			var assembly = AssemblySerializer.Instance.Deserialize(deserializer);
			string typeName = stream.ReadString();
			var result = assembly.GetType(typeName, true);
			return result;
		}

		private static readonly Type _forType = typeof(TypeSerializer).GetType();
		Type IItemSerializer.ForType
		{
			get
			{
				return _forType;
			}
		}

		void IItemSerializer.Serialize(ConfigurableSerializerBase serializer, object item)
		{
			Type type = (Type)item;
			Serialize(serializer, type);
		}
		object IItemSerializer.Deserialize(ConfigurableSerializerBase deserializer)
		{
			return Deserialize(deserializer);
		}
	}
}
