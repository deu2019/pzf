﻿using Pfz.Extensions;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializes a string.
	/// </summary>
	public sealed class StringSerializer:
		ItemSerializer<string>
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly StringSerializer Instance = new StringSerializer();

		private StringSerializer()
		{
		}

		/// <summary>
		/// Serializes a string.
		/// </summary>
		public override void Serialize(ConfigurableSerializerBase serializer, string item)
		{
			serializer.Stream.WriteString(item);
		}

		/// <summary>
		/// Deserializes a string.
		/// </summary>
		public override string Deserialize(ConfigurableSerializerBase deserializer)
		{
			return deserializer.Stream.ReadString();
		}
	}
}
