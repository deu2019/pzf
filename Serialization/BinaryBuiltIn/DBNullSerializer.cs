﻿using System;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializes a DBNull value.
	/// </summary>
	public sealed class DBNullSerializer:
		ItemSerializer<DBNull>
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly DBNullSerializer Instance = new DBNullSerializer();

		private DBNullSerializer()
		{
		}

		/// <summary>
		/// Does nothing, as the type is known.
		/// </summary>
		public override void Serialize(ConfigurableSerializerBase serializer, DBNull item)
		{
		}

		/// <summary>
		/// Returns DBNull.Value.
		/// </summary>
		public override DBNull Deserialize(ConfigurableSerializerBase deserializer)
		{
			return DBNull.Value;
		}
	}
}
