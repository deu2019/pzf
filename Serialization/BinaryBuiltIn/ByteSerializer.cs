﻿using Pfz.Extensions;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializes a single byte.
	/// </summary>
	public sealed class ByteSerializer:
		ItemSerializer<byte>
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly ByteSerializer Instance = new ByteSerializer();

		private ByteSerializer()
		{
		}

		/// <summary>
		/// Serializes the byte directly.
		/// </summary>
		public override void Serialize(ConfigurableSerializerBase serializer, byte item)
		{
			serializer.Stream.WriteByte(item);
		}

		/// <summary>
		/// Reads a byte directly.
		/// </summary>
		public override byte Deserialize(ConfigurableSerializerBase deserializer)
		{
			return deserializer.Stream.ReadByteOrThrow();
		}
	}
}
