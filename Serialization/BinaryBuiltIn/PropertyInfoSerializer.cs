﻿using System;
using System.Reflection;
using System.Runtime.Serialization;
using Pfz.Extensions;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializer for PropertyInfo.
	/// </summary>
	public sealed class PropertyInfoSerializer:
		IItemSerializer
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly PropertyInfoSerializer Instance = new PropertyInfoSerializer();

		private PropertyInfoSerializer()
		{
		}

		/// <summary>
		/// Serializes the PropertyInfo.
		/// </summary>
		public void Serialize(ConfigurableSerializerBase serializer, PropertyInfo item)
		{
			TypeSerializer.Instance.Serialize(serializer, item.ReflectedType);
			serializer.Stream.WriteString(item.Name);
		}

		/// <summary>
		/// Deserializes the PropertyInfo.
		/// </summary>
		public PropertyInfo Deserialize(ConfigurableSerializerBase deserializer)
		{
			Type type = TypeSerializer.Instance.Deserialize(deserializer);
			string name = deserializer.Stream.ReadString();

			var result = type.GetProperty(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
			if (result == null)
				throw new SerializationException("Can't find property " + name + " on type " + type.FullName);

			return result;
		}

		private static readonly Type _forType = ReflectionHelper.GetProperty(() => AppDomain.CurrentDomain).GetType();
		Type IItemSerializer.ForType
		{
			get
			{
				return _forType;
			}
		}

		void IItemSerializer.Serialize(ConfigurableSerializerBase serializer, object item)
		{
			Serialize(serializer, (PropertyInfo)item);
		}

		object IItemSerializer.Deserialize(ConfigurableSerializerBase deserializer)
		{
			return Deserialize(deserializer);
		}
	}
}
