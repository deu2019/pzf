﻿using Pfz.Extensions;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializes a single boolean.
	/// </summary>
	public sealed class BooleanSerializer:
		ItemSerializer<bool>
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly BooleanSerializer Instance = new BooleanSerializer();

		private BooleanSerializer()
		{
		}

		/// <summary>
		/// Serializes a single boolean.
		/// </summary>
		public override void Serialize(ConfigurableSerializerBase serializer, bool item)
		{
			if (item)
				serializer.Stream.WriteByte(1);
			else
				serializer.Stream.WriteByte(0);
		}

		/// <summary>
		/// Deserializes a single boolean.
		/// </summary>
		public override bool Deserialize(ConfigurableSerializerBase deserializer)
		{
			return deserializer.Stream.ReadByteOrThrow() != 0;
		}
	}
}
