﻿using System;
using Pfz.Extensions;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializer for Int64. Uses compressed format.
	/// </summary>
	public sealed class Int64Serializer:
		ItemSerializer<long>
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly Int64Serializer Instance = new Int64Serializer();

		private Int64Serializer()
		{
		}

		/// <summary>
		/// Serializes a long value using a compressed format.
		/// </summary>
		public override void Serialize(ConfigurableSerializerBase serializer, long value)
		{
			serializer.Stream.WriteCompressedInt64(value);
		}

		/// <summary>
		/// Deserializes a compressed long value.
		/// </summary>
		public override long Deserialize(ConfigurableSerializerBase deserializer)
		{
			return deserializer.Stream.ReadCompressedInt64();
		}
	}
}
