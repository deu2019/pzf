﻿using System.Collections.Generic;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializes a key value pair.
	/// </summary>
	public sealed class PairSerializer<TKey, TValue>:
		ItemSerializer<KeyValuePair<TKey, TValue>>
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly PairSerializer<TKey, TValue> Instance = new PairSerializer<TKey, TValue>();

		private PairSerializer()
		{
		}

		/// <summary>
		/// Serializes a pair.
		/// </summary>
		public override void Serialize(ConfigurableSerializerBase serializer, KeyValuePair<TKey, TValue> item)
		{
			serializer.InnerSerialize(item.Key, typeof(TKey));
			serializer.InnerSerialize(item.Value, typeof(TValue));
		}

		/// <summary>
		/// Deserializes a pair.
		/// </summary>
		public override KeyValuePair<TKey, TValue> Deserialize(ConfigurableSerializerBase deserializer)
		{
			var key = (TKey)deserializer.InnerDeserialize(typeof(TKey));
			var value = (TValue)deserializer.InnerDeserialize(typeof(TValue));
			var result = new KeyValuePair<TKey, TValue>(key, value);
			return result;
		}
	}
}
