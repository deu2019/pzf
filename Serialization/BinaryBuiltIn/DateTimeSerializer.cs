﻿using System;
using Pfz.Extensions;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializer for DateTime.
	/// </summary>
	public sealed class DateTimeSerializer:
		ItemSerializer<DateTime>
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly DateTimeSerializer Instance = new DateTimeSerializer();

		private DateTimeSerializer()
		{
		}

		/// <summary>
		/// Serializes the DateTime.
		/// </summary>
		public override void Serialize(ConfigurableSerializerBase serializer, DateTime item)
		{
			serializer.Stream.WriteCompressedInt64(item.Ticks);
		}

		/// <summary>
		/// Deserializes a DateTime.
		/// </summary>
		public override DateTime Deserialize(ConfigurableSerializerBase deserializer)
		{
			long ticks = deserializer.Stream.ReadCompressedInt64();
			return new DateTime(ticks);
		}
	}
}
