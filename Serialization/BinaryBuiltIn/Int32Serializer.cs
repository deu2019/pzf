﻿using System;
using Pfz.Extensions;

namespace Pfz.Serialization.BinaryBuiltIn
{
	/// <summary>
	/// Serializer for Int32. Uses compressed format.
	/// </summary>
	public sealed class Int32Serializer:
		ItemSerializer<int>
	{
		/// <summary>
		/// Gets the singleton instance.
		/// </summary>
		public static readonly Int32Serializer Instance = new Int32Serializer();

		private Int32Serializer()
		{
		}

		/// <summary>
		/// Serializes the value in a compressed format.
		/// </summary>
		public override void Serialize(ConfigurableSerializerBase serializer, int value)
		{
			serializer.Stream.WriteCompressedInt32(value);
		}

		/// <summary>
		/// Deserializes a compressed int.
		/// </summary>
		public override int Deserialize(ConfigurableSerializerBase deserializer)
		{
			return deserializer.Stream.ReadCompressedInt32();
		}
	}
}
