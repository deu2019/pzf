﻿using System;

namespace Pfz.Serialization
{
	/// <summary>
	/// Abstract class that implements IItemSerializer and has typed Serialize and Deserialize
	/// methods.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class ItemSerializer<T>:
		IItemSerializer
	{
		/// <summary>
		/// Method that must be overriden to serialize an item.
		/// </summary>
		public abstract void Serialize(ConfigurableSerializerBase serializer, T item);

		/// <summary>
		/// Method that must be overriden to deserialize an item.
		/// </summary>
		public abstract T Deserialize(ConfigurableSerializerBase deserializer);

		Type IItemSerializer.ForType
		{
			get
			{
				return typeof(T);
			}
		}

		void IItemSerializer.Serialize(ConfigurableSerializerBase serializer, object item)
		{
			Serialize(serializer, (T)item);
		}
		object IItemSerializer.Deserialize(ConfigurableSerializerBase deserializer)
		{
			return Deserialize(deserializer);
		}
	}
}
