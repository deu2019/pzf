﻿
namespace Pfz.Serialization
{
	/// <summary>
	/// Class returned when deserializing an item if it is a reference to an object not yet deserialized.
	/// </summary>
	public sealed class SerializationReference
	{
		internal SerializationReference(int id)
		{
			ReferenceId = id;
		}

		/// <summary>
		/// Gets the Id of the referenced object.
		/// </summary>
		public int ReferenceId { get; private set; }
	}
}
