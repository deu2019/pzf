﻿using System;

namespace Pfz.Serialization
{
	/// <summary>
	/// Interface that must be implemented by classes capable of serializing
	/// specific items.
	/// </summary>
	public interface IItemSerializer
	{
		/// <summary>
		/// Gets the type of the item this class is able to serialize.
		/// </summary>
		Type ForType { get; }

		/// <summary>
		/// Must be implemented to serialize an item.
		/// </summary>
		void Serialize(ConfigurableSerializerBase serializer, object item);

		/// <summary>
		/// Must be implemented to deserialize an item.
		/// </summary>
		object Deserialize(ConfigurableSerializerBase deserializer);
	}
}
